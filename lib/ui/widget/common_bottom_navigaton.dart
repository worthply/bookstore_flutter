import 'package:book_store/core/util/hex_color.dart';
import 'package:flutter/material.dart';
class CommonBottomNavigation extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: HexColor("#FFFFFF"),
      child: Container(
        margin: EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(15))
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 20,right: 20),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Image(
                    image: AssetImage("images/gallery_icon.png"),
                  ),
                  Text("Gallery",style: TextStyle(color: HexColor("#A8ADB8")),)
                ],
              ),
              Text("Add",style: TextStyle(color: HexColor("#A8ADB8")),),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Image(
                    image: AssetImage("images/profile.png"),
                  ),
                  Text("Profile",style: TextStyle(color: HexColor("#A8ADB8")),)
                ],
              ),
            ],
          ),
        ),
      ),
      shape: CircularNotchedRectangle(),

    );
  }

}