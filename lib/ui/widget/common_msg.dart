import 'package:flutter/material.dart';
class CommonMsg extends StatelessWidget{
  final String msg;

  CommonMsg(this.msg);

  @override
  Widget build(BuildContext context) {
  return Center(
    child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(msg,style: Theme.of(context).textTheme.title,),
    ),
    );
  }


}