import 'package:book_store/ui/views/AudioView.dart';
import 'package:book_store/ui/views/bookshelf_view.dart';
import 'package:book_store/ui/views/content_tab_view.dart';
import 'package:book_store/ui/views/details_view.dart';
import 'package:book_store/ui/views/filter_view.dart';
import 'package:book_store/ui/views/menu_view.dart';
import 'package:book_store/ui/views/my_post_view.dart';
import 'package:book_store/ui/views/notification_view.dart';
import 'package:book_store/ui/views/reading_now_view.dart';
import 'package:book_store/ui/views/read_view.dart';
import 'package:book_store/ui/views/search_author_view.dart';
import 'package:book_store/ui/views/search_books_view.dart';
import 'package:book_store/ui/views/search_category_view.dart';
import 'package:book_store/ui/views/search_chart_view.dart';
import 'package:book_store/ui/views/feed_story_view.dart';
import 'package:book_store/ui/views/feed_view.dart';
import 'package:book_store/ui/views/bottom_tab_view.dart';
import 'package:book_store/ui/views/home_view.dart';
import 'package:book_store/ui/views/library_view.dart';
import 'package:book_store/ui/views/search_more_view.dart';
import 'package:book_store/ui/views/my_view.dart';
import 'package:book_store/ui/views/search_result_view.dart';
import 'package:book_store/ui/views/search_view.dart';
import 'package:book_store/ui/views/sign_up_view.dart';
import 'package:book_store/ui/views/signin_view.dart';
import 'package:book_store/ui/views/splash_view.dart';
import 'package:book_store/ui/views/welcome_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

const String routeInitial = "splash";
const String routeWelcome = "welcome";
const String routeSignIn = "sign_in";
const String routeSignUp = "sign_up";
const String routeTabScreen = "tab_view";
const String routeHomeView = "home_view";
const String routeFeedView = "feed_view";
const String routeSearchView = "search_view";
const String routeLibraryView = "library_view";
const String routeMyView = "my_view";
const String routeStoryView = "feed_story_view";
const String routeCategorySearchView = "category_search_view";
const String routeChartSearchView = "chart_search_view";
const String routeMoreSearchView = "more_search_view";
const String routeBookSearchView = "book_search_view";
const String routeAuthorSearchView = "author_search_view";
const String routeAuthorKeywordView = "keyword_search_view";
const String routeSearchResultView = "search_result_view";
const String routeFilterView = "filter_view";
const String routeReadingNowView = "reading_now_view";
const String routeBookShelfView = "book_shelf_view";
const String routeMyPostView = "my_post_view";
const String routeDetailsView = "details_view";
const String routeNotificationView = "notification_view";
const String routeReadView = "read_view";
const String routeAudioView = "Audio_view";
const String routeMenuView = "menu_view";
const String routeContentTabView = "content_tab_view";

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case routeInitial:
        return MaterialPageRoute(builder: (_) => SplashView());

      case routeWelcome:
        return MaterialPageRoute(builder: (_) => WelcomeView());

      case routeSignIn:
        return MaterialPageRoute(builder: (_) => SignInView());

      case routeSignUp:
        return MaterialPageRoute(builder: (_) => SignUpView());

      case routeTabScreen:
        return MaterialPageRoute(builder: (_) => BottomTabView());

      case routeHomeView:
        return MaterialPageRoute(builder: (_) => HomeView());

      case routeFeedView:
        return MaterialPageRoute(builder: (_) => FeedView());

      case routeSearchView:
        return MaterialPageRoute(builder: (_) => SearchView());

      case routeLibraryView:
        return MaterialPageRoute(builder: (_) => LibraryView());

      case routeMyView:
        return MaterialPageRoute(builder: (_) => MyView());

      case routeStoryView:
        return MaterialPageRoute(builder: (_) => FeedStoryView());

      case routeCategorySearchView:
        return MaterialPageRoute(builder: (_) => CategorySearchView());

      case routeChartSearchView:
        return MaterialPageRoute(builder: (_) => ChartSearchView());

      case routeMoreSearchView:
        return MaterialPageRoute(builder: (_) => MoreSearchView());

      case routeBookSearchView:
        return MaterialPageRoute(builder: (_) => SearchBookView());

      case routeAuthorSearchView:
        return MaterialPageRoute(builder: (_) => SearchAuthorView());

      case routeAuthorSearchView:
        return MaterialPageRoute(builder: (_) => SearchAuthorView());

      case routeSearchResultView:
        return MaterialPageRoute(builder: (_) => SearchResultView());

      case routeFilterView:
        return MaterialPageRoute(builder: (_) => FilterView());

      case routeReadingNowView:
        return MaterialPageRoute(builder: (_) => ReadingNowView());

      case routeBookShelfView:
        return MaterialPageRoute(builder: (_) => BookShelfView());

      case routeMyPostView:
        return MaterialPageRoute(builder: (_) => MyPostView());

      case routeDetailsView:
        return MaterialPageRoute(builder: (_) => DetailsView());

      case routeNotificationView:
        return MaterialPageRoute(builder: (_) => NotificationView());

      case routeReadView:
        return MaterialPageRoute(builder: (_) => ReadView());

      case routeAudioView:
        return MaterialPageRoute(builder: (_) => AudioView());

      case routeMenuView:
        return MaterialPageRoute(builder: (_) => MenuView(settings.arguments));

      case routeContentTabView:
        return MaterialPageRoute(builder: (_) => ContentTabView());

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
