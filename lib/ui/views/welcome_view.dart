import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/util/utility.dart';
import 'package:book_store/core/viewmodels/welcome_mView.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class WelcomeView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return WelcomeViewState() ;
  }

}

class WelcomeViewState extends State<WelcomeView > {
  @override
  Widget build(BuildContext context) {
    return BaseView<WelcomeModelView>(
      onModelReady: (model) {
        Utility.networkStatus();
      },
      builder: (context, model, child) {
        return Scaffold(
          backgroundColor: HexColor("#00D6D8"),
          body: _body(),
        );
      },
    );
  }
  _body() {
    return Container(
      child: Stack(
        children: <Widget>[
          Center(child: Image.asset("images/3x/welcome_bg.png",fit: BoxFit.fill,)),
          Align(
            alignment: Alignment(.5,.5),
            child: InkWell(
              onTap: (){
                Navigator.pushNamed(context, routeSignIn);
              },
              child: Container(
                width: double.infinity,
                height: 55,
                margin: EdgeInsets.only(left: 30,right: 30),
                decoration:
                BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#FFFFFF')),
                child: Center(
                  child: Text(
                    "Sign In",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: HexColor(
                          "#00D6D8",
                        ),
                        fontSize: 22),
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment(.5,.75),
            child: InkWell(
              onTap: (){
                Navigator.pushNamed(context, routeSignUp);
              },
              child: Container(
                width: double.infinity,
                height: 55,
                margin: EdgeInsets.only(left: 30,right: 30),
                decoration:
                BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#FFEB00')),
                child: Center(
                  child: Text(
                    "Sign Up",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: HexColor(
                          "#00D6D8",
                        ),
                        fontSize: 22),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );

  }
}