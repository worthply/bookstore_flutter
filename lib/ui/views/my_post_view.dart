import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/my_post_view_model.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class MyPostView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    
    return MyPostViewState();
  }
}

class MyPostViewState extends State<MyPostView> {
  @override
  Widget build(BuildContext context) {
    
    return BaseView<MyPostViewModel>(
      onModelReady: (model){
        
      },
      builder: (context,model,child){
        return Scaffold(
          body: _body(),
        );
      },
      
    );
  }

  _body() {
     return ListView(
      padding: EdgeInsets.all(8),
      children: <Widget>[
        SizedBox(height: 30,),
        Container(
          height: ScreenUtil().setHeight(532),
          width: ScreenUtil().setWidth(335),
          child: ListView.builder(
              scrollDirection: Axis.vertical,
              physics: BouncingScrollPhysics(),
              itemCount: 5,
              itemBuilder: (BuildContext context, int index) {
                return  ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Container(
                    height: ScreenUtil().setHeight(532),
                    width: ScreenUtil().setWidth(335),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          height: ScreenUtil().setHeight(335),
                          width: double.infinity,
                          child: CachedNetworkImage(
                            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIXsc3UJfDcHP6aluRV7m0As_rmbqtZwJMGS9p-D6MZ7b-3UMCiQ&s",
                            fit: BoxFit.fill,
                            placeholder: (context, url) =>
                                CommonLoader(
                                  size: small,
                                ),
                            errorWidget: (context, url, error) =>
                                Icon(
                                  Icons.broken_image,
                                ),
                          ),

                        ),
                        Align(
                          alignment: Alignment(0,-.5),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20,right: 20),
                            child: Text('“I’ve missed you, too.” Though he was sheepish, I thought I saw something else in his eyes. After three years, he probably didn’t love me anymore. Yet as I stared deeper into the black pools of his pupils, I allowed myself to consider that maybe he did.',
                              style: TextStyle(
                                  color: HexColor('#FFFFFF'),
                                  fontSize: ScreenUtil().setSp(17)
                              ),),
                          ),
                        ),
                        Align(
                          alignment: Alignment(-.9,.4),
                          child: Container(
                            height: ScreenUtil().setHeight(120),
                            width:ScreenUtil().setWidth(80),
                            child: CachedNetworkImage(
                              imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSizTDzO-brUiftTqrOCO2cly7s-degjbE1RqYnnyIpN6GXqDqq&s",
                              fit: BoxFit.fill,
                              placeholder: (context, url) =>
                                  CommonLoader(
                                    size: small,
                                  ),
                              errorWidget: (context, url, error) =>
                                  Icon(
                                    Icons.broken_image,
                                  ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment(0,.4),
                          child: Text('The Case for Trump',
                            style: TextStyle(
                                fontSize: ScreenUtil().setSp(15),
                                fontWeight: FontWeight.bold,
                                color: HexColor('#242126')
                            ),),
                        ),
                        Align(
                          alignment: Alignment(0,.5),
                          child: Text('by Victor Davis Ha…',
                            style: TextStyle(
                                fontSize: ScreenUtil().setSp(15),
                                fontWeight: FontWeight.bold,
                                color: HexColor('#D1DDDF')
                            ),),
                        ),
                      ],
                    ),
                  ),
                );
              }),
        )
      ],
    );
  }
}