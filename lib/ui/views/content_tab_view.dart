import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/content_tab_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class ContentTabView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return ContentTabViewState();
  }
}

class ContentTabViewState extends State<ContentTabView> {
  @override
  Widget build(BuildContext context) {

    return BaseView<ContentTabViewModel>(
      onModelReady: (model){},
      builder: (context,model,child){
        return Scaffold(
          body: _body(),

        );
      },
    );
  }

  _body() {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 30,),
          Expanded(
            child: Container(
              child: ListView.builder(
                  itemCount: 5,
                  itemBuilder: (context,index){
                    return Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text('Cover', style: TextStyle(
                              color: HexColor('#242126'),
                              fontSize: ScreenUtil().setSp(15),
                              fontWeight: FontWeight.bold
                          ),),
                          Text('00:00', style: TextStyle(
                              color: HexColor('#AFC1C4'),
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.bold
                          ),),
                        ],
                      ),
                    );
                  }),
            ),
          ),

        ],
      ),
    );
  }
}