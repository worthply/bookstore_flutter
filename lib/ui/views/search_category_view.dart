import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/search_category_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CategorySearchView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return CategorySearchViewState();
  }
}

class CategorySearchViewState  extends State<CategorySearchView>{
  @override
  Widget build(BuildContext context) {
    return BaseView<CategorySearchViewModel>(
      builder:(context,model,child){
        return Scaffold(
          body: _body(),
        );
      } ,
    );
  }

  _body() {
    return GridView.builder(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: 20,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              InkWell(
                onTap: () {
                },
                child: Container(
                  height: ScreenUtil().setHeight(180),
                  width: ScreenUtil().setWidth(160),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Container(
                      decoration: BoxDecoration(
                        color: HexColor('#A59286'),
                      ),
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 10),
                            Text("Arts & Photography",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: HexColor('#FFFFFF'),
                                  fontSize: ScreenUtil().setSp(12),
                                  fontWeight: FontWeight.bold
                              ),),
                            SizedBox(height: 10,),
                            ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Container(
                                height: ScreenUtil().setHeight(120),
                                width: ScreenUtil().setWidth(80),
                                child: CachedNetworkImage(
                                  imageUrl: "https://images-na.ssl-images-amazon.com/images/I/813TSQsp2wL._AC_UL320_SR220,320_.jpg",
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) =>
                                      CommonLoader(
                                        size: small,
                                      ),
                                  errorWidget: (context, url, error) =>
                                      Icon(
                                        Icons.broken_image,
                                      ),
                                ),
                              ),
                            ),
                          ],
                        ),

                      ),
                    ),
                  ),
                ),
              ),

            ],
          );
        });
  }

}
