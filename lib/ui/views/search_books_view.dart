import 'package:book_store/core/viewmodels/search_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:flutter/material.dart';
class SearchBookView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return SearchBookViewState ();
  }

}

class SearchBookViewState extends State<SearchBookView> {
  @override
  Widget build(BuildContext context) {

    return BaseView<SearchViewModel>(
      builder:(context,model,child){
      return Scaffold(
        body: _body(),
      );
      },
    );
  }

  _body() {
  }
}