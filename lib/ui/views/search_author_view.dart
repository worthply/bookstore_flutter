import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/search_author_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class SearchAuthorView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return SearchAuthorViewState();
  }

}

class SearchAuthorViewState extends State<SearchAuthorView>{
  @override
  Widget build(BuildContext context) {
    return BaseView<SearchAuthorViewModel>(
      builder: (context,model,child){
        return Scaffold(
          body: _body(),
        );
      },
    );
  }

  _body() {
    return ListView.builder(
        padding: EdgeInsets.fromLTRB(15, 10, 15, 0),
        scrollDirection: Axis.vertical,
        itemCount: 20,
        physics: BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Stack(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    SizedBox(width: 30,),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Container(
                        height: ScreenUtil().setHeight(70),
                        width: ScreenUtil().setHeight(70),
                        child: CachedNetworkImage(
                          imageUrl: "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                          fit: BoxFit.fill,
                          placeholder: (context, url) =>
                              CommonLoader(
                                size: small,
                              ),
                          errorWidget: (context, url, error) =>
                              Icon(
                                Icons.broken_image,
                              ),
                        ),
                      ),
                    ),
                    SizedBox(width: 20,),
                    Text('Adam Kay',style: TextStyle(
                        color: HexColor('#242126'),
                        fontSize: ScreenUtil().setWidth(17),
                        fontWeight: FontWeight.bold
                    ),)
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(right: 10),
                  child: Container(
                    padding: EdgeInsets.all(2),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: HexColor('#00D6D8'),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Container(
                        height: ScreenUtil().setHeight(60),
                        width: ScreenUtil().setHeight(60),
                        child: Center(child: Text(index.toString(),style: TextStyle(
                            color:Colors.white,
                            fontSize:ScreenUtil().setSp(17)
                        ),)),
                      ),
                    ),
                  ),
                ),
              ],

            ),
          );
        }
    );
  }
}