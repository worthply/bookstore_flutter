import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/audio_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:marquee/marquee.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class AudioView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return AudioViewState ();
  }
}

class AudioViewState extends State<AudioView> {

  @override
  Widget build(BuildContext context) {
    return BaseView<AudioViewModel>(
      builder:(context,model,child){
        return Scaffold(
          backgroundColor: HexColor('#6DB9D0'),
          body: _body(model),
          appBar: model.showSetting==false?appBarView(model):null);
    } ,
    );
  }

  _body(AudioViewModel model) {
    return Container(
      child: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              SizedBox(height: 15,),
              LinearPercentIndicator(
                animation: true,
                lineHeight: ScreenUtil().setHeight(2),
                animationDuration: 2000,
                percent: 0.24,
                progressColor: HexColor('#FFFFFF'),
              ),
              SizedBox(height: 15,),
              Text('The Unwinding of the Miracle',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: HexColor('#FFFFFF'),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17)
                ),),
              SizedBox(height: 10,),
              Text('Julie Yip-Williams ',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: HexColor('#707070').withOpacity(.5),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17)
                ),),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: ScreenUtil().setWidth(120),
                    margin: EdgeInsets.only(left: 20, right: 20),
                    child: Stack(
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Container(
                            height: ScreenUtil().setHeight(300),
                            width: ScreenUtil().setWidth(200),
                            decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(.5)
                            ),
                            child: CachedNetworkImage(
                              imageUrl: "https://images-na.ssl-images-amazon.com/images/I/41pps%2BZIgOL._SX321_BO1,204,203,200_.jpg",
                              fit: BoxFit.fill,
                              placeholder: (context, url) =>
                                  CommonLoader(
                                    size: small,
                                  ),
                              errorWidget: (context, url, error) =>
                                  Icon(
                                    Icons.broken_image,
                                  ),
                            ),
                          ),
                        ),
                      ],

                    ),
                  )
                ],
              ),
              SizedBox(height: 30,),
              LinearPercentIndicator(
                animation: true,
                padding: EdgeInsets.only(left: 15,right: 15),
                lineHeight: ScreenUtil().setHeight(2),
                animationDuration: 2000,
                percent: 0.24,
                progressColor: HexColor('#FFFFFF'),
              ),
              SizedBox(height: 10,),
              Container(
                height: 80,
                width: double.infinity,
                padding: EdgeInsets.only(top: 25, left: 10, right: 10, bottom: 20),
                margin: EdgeInsets.only(
                  right: 10,
                  left: 10,
                ),
                child: showMarque(model),
                //child: _buildMarquee(model)
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset('images/backwordPrev10s.png'),
                  SizedBox(width: 20,),
                  Image.asset('images/paus.png'),
                  SizedBox(width: 20,),
                  Image.asset('images/forwordPrev10s.png'),

                ],
              )
            ],
          ),
          model.showSetting?settingView(model):Container()
        ],

      ),

    );
  }

  appBarView(AudioViewModel model) {
    return AppBar(
      elevation: 0,
      backgroundColor: HexColor('#6DB9D0'),
      centerTitle: true,
      leading: InkWell(
          onTap: () {
            if(model.showSetting==false){
              model.showSetting=true;
            }
            else{
              model.showSetting=false;
            }
          },
          child: Image.asset('images/setting.png')),
      title: Text(
        'Audio',
        style: TextStyle(color: HexColor('#FFFFFF'),
        fontSize: ScreenUtil().setSp(17),
        fontWeight: FontWeight.bold),

      ),
      actions: <Widget>[
        InkWell(
          onTap: (){
            Navigator.pop(context);
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 8),
            child: Icon(Icons.clear),
          ),
        ),
      ],
    );
  }

  showMarque(AudioViewModel model){
    return Marquee(
      text: 'Bundling has also been used to segment the used book market. Each combination of a textbook and supplemental items receives a separate ISBN. A single textbook could therefore have dozens of ISBNs that denote different combinations of supplements packaged with that particular book. When a bookstore attempts to track down used copies of textbooks, they will search for the ISBN the course instructor orders, which will locate only a subset of the copies of the textbook',
      style: TextStyle(color: Colors.white,fontSize: ScreenUtil().setSp(21),),
      scrollAxis: Axis.vertical,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      blankSpace: 30.0,
      velocity: 5.0,
      pauseAfterRound: Duration(seconds: 0),
      startPadding: 10.0,
      // accelerationDuration: Duration(seconds: 30),
      accelerationCurve: Curves.linear,
      //decelerationDuration: Duration(seconds: 0),
      //decelerationCurve: Curves.easeOut,
    );
  }

  settingView(AudioViewModel model){
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(25),bottomLeft: Radius.circular(25)), color: HexColor('#FFFFFF')
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 30,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Audio Setting',
              style: TextStyle(
                fontSize: ScreenUtil().setSp(24),
                fontWeight: FontWeight.bold,
                color: HexColor('#242126')
              ),),
              SizedBox(height: 15,),
              InkWell(
                onTap: (){
                  if(model.showSetting==false){
                    model.showSetting=true;
                  }
                  else{
                    model.showSetting=false;
                  }
                },
                child: Text('Cancel',
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(17),
                  fontWeight: FontWeight.w100,
                  color: HexColor('#242126')
                ),),
              ),
            ],
          ),
          SizedBox(height: 25,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Speed',
              style: TextStyle(
                fontSize: ScreenUtil().setSp(17),
                fontWeight: FontWeight.bold,
                color: HexColor('#242126')
              ),),



            ],
          ),
          SizedBox(height: 15,),
          Container(
            width: double.infinity,
            height: 55,
            decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#EDEFF3')),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: (){
                      model.speed=0;
                    },
                    child: Container(
                      height: 55,
                      margin: EdgeInsets.all(2),
                      decoration:
                      BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: model.speed==0?HexColor('#FFFFFF'):Colors.transparent),
                      child: Center(
                        child: Text(
                          "X0.5",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: model.speed==0?HexColor(
                                "#00D6D8",
                              ):HexColor('#707070'),
                              fontSize: ScreenUtil().setSp(15),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: (){
                      model.speed=1;
                    },
                    child: Container(
                      margin: EdgeInsets.all(2),
                      height: 55,
                      decoration:
                      BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: model.speed==1?HexColor('#FFFFFF'):Colors.transparent),
                      child: Center(
                        child: Text(
                          "X1.0",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: model.speed==1?HexColor(
                                "#00D6D8",
                              ):HexColor('#707070'),
                              fontSize: ScreenUtil().setSp(17),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: (){
                      model.speed=2;
                    },
                    child: Container(
                      margin: EdgeInsets.all(2),
                      height: 55,
                      decoration:
                      BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: model.speed==2?HexColor('#FFFFFF'):Colors.transparent),
                      child: Center(
                        child: Text(
                          "X2.0",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: model.speed==2?HexColor(
                                "#00D6D8",
                              ):HexColor('#707070'),
                              fontSize: ScreenUtil().setSp(17),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Voice',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(17),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                ),),



            ],
          ),
          SizedBox(height: 15,),
          Container(
            width: double.infinity,
            height: 55,
            padding: EdgeInsets.only(left: 15,right: 15),
            decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: Colors.white,
            border: Border.all(
              color: HexColor('#D1DDDF',)
            )),

            child: Center(
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                  isExpanded: true,
                  isDense: false,
                  hint: Text('Voice'),
                  style: TextStyle(
                    color: HexColor('#00D6D8'),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(15),
                  ),
                  value: model.voice,
                  onChanged: (value) {
                    setState(() {
                      model.voice = value;
                    });
                  },
                  items: [
                    DropdownMenuItem(
                      value: "0",
                      child: Text(
                        "James",
                      ),
                    ),
                    DropdownMenuItem(
                      value: "1",
                      child: Text(
                        "July",
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 15,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Background Mode',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(17),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                ),),

            ],
          ),
          SizedBox(height: 15,),
          Container(
            width: double.infinity,
            height: 55,
            decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#EDEFF3')),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: (){
                      model.backgroundMode=true;
                    },
                    child: Container(
                      height: 55,
                      margin: EdgeInsets.all(2),
                      decoration:
                      BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: model.backgroundMode?HexColor('#FFFFFF'):Colors.transparent),
                      child: Center(
                        child: Text(
                          "On",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: model.backgroundMode?HexColor(
                                "#00D6D8",
                              ):HexColor('#707070'),
                              fontSize: ScreenUtil().setSp(15),
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: (){
                      model.backgroundMode =false;
                    },
                    child: Container(
                      margin: EdgeInsets.all(2),
                      height: 55,
                      decoration:
                      BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: model.backgroundMode==false?HexColor('#FFFFFF'):Colors.transparent),
                      child: Center(
                        child: Text(
                          "Off",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: model.backgroundMode==false?HexColor(
                                "#00D6D8",
                              ):HexColor('#707070'),
                              fontSize: ScreenUtil().setSp(17),
                              fontWeight: FontWeight.bold),

                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 30,),
          InkWell(
            onTap: (){
              if(model.showSetting==false){
                model.showSetting=true;
              }
              else{
                model.showSetting=false;
              }
            },
            child: Container(
              width: double.infinity,
              height: 55,
              decoration:
              BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#00D6D8').withOpacity(.1)),
              child: Center(
                child: Text('Done',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17),
                    color: HexColor('#00D6D8')

                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 20,)
        ],
      ),
    );
  }

}