import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/filter_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class FilterView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FilterViewState();
  }
}

class FilterViewState extends State<FilterView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<FilterViewModel>(
      onModelReady: (model) {},
      builder: (context, model, child) {
        return Scaffold(
          body: _body(model),
        );
      },
    );
  }

  _body(FilterViewModel model) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Filter',
                style: TextStyle(
                  color: HexColor('#242126'),
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil().setSp(24),
                ),
              ),
              InkWell(
                onTap: (){
                  Navigator.pop(context);
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                      color: HexColor('#242126'),
                      fontWeight: FontWeight.w500,
                      fontSize: ScreenUtil().setSp(17),
                    ),
                  ),
                ),
              )
            ],
          ),
          Expanded(
            child: ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                sortByView(model),
                SizedBox(height: 20,),
                departmentView(model),
                SizedBox(height: 20,),
                customerReview(model),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        height: 50,
                        decoration:
                        BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#00D6D8').withOpacity(.1)),
                        child: Center(
                          child: Text(
                            "Apply",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: HexColor(
                                  "#00D6D8",
                                ),
                                fontSize: 22),
                          ),
                        ),
                      ),
                    )
                  ],

                ),
                SizedBox(height: 20,),
              ],
            ),
          )

        ],
      ),
    );
  }

  sortByView(FilterViewModel model) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Sort by',
              style: TextStyle(
                color: HexColor('#D1DDDF'),
                fontWeight: FontWeight.bold,
                fontSize: ScreenUtil().setSp(12),
              ),
            ),
            Image.asset('images/drop_down.png')
          ],
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.boolFeatured) {
              model.boolFeatured = false;
            } else {
              model.boolFeatured = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.boolFeatured ? 'images/dot_checked.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Featured',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.boolLowToHigh) {
              model.boolLowToHigh = false;
            } else {
              model.boolLowToHigh = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.boolLowToHigh ? 'images/dot_checked.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Price: Low to High',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.boolHighToLow) {
              model.boolHighToLow = false;
            } else {
              model.boolHighToLow = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.boolHighToLow ? 'images/dot_checked.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Price: High to Low',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.boolCustomerReview) {
              model.boolCustomerReview = false;
            } else {
              model.boolCustomerReview = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.boolCustomerReview ? 'images/dot_checked.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Avg. Customer Review',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.boolPublicationDate) {
              model.boolPublicationDate = false;
            } else {
              model.boolPublicationDate = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.boolPublicationDate ? 'images/dot_checked.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Publication Date',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
      ],
    );
  }

  departmentView(FilterViewModel model) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Department',
              style: TextStyle(
                color: HexColor('#D1DDDF'),
                fontWeight: FontWeight.bold,
                fontSize: ScreenUtil().setSp(12),
              ),
            ),
            Image.asset('images/drop_down.png')
          ],
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.biographies) {
              model.biographies = false;
            } else {
              model.biographies = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.biographies ? 'images/selected_checkbox.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Biographies & Memoirs',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.literature) {
              model.literature = false;
            } else {
              model.literature = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.literature ? 'images/selected_checkbox.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Literature & Fiction',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.literary) {
              model.literary = false;
            } else {
              model.literary = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.literary ? 'images/selected_checkbox.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Literary',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.memories) {
              model.memories = false;
            } else {
              model.memories = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.memories ? 'images/selected_checkbox.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Memoirs',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.suspense) {
              model.suspense = false;
            } else {
              model.suspense = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.suspense ? 'images/selected_checkbox.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              Text(
                'Suspense',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
      ],
    );
  }

  customerReview(FilterViewModel model) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Avg. Customer Review',
              style: TextStyle(
                color: HexColor('#D1DDDF'),
                fontWeight: FontWeight.bold,
                fontSize: ScreenUtil().setSp(12),
              ),
            ),
            Image.asset('images/drop_down.png')
          ],
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.ratFour) {
              model.ratFour = false;
            } else {
              model.ratFour = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.ratFour ? 'images/selected_checkbox.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              RatingBar(
                initialRating: 4,
                minRating: 4,
                maxRating: 4,
                itemSize: ScreenUtil().setWidth(17),
                direction: Axis.horizontal,
                itemCount: 5,
                itemBuilder: (context, _) =>
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
              ),
              SizedBox(width: 5,),
              Text(
                '& Up',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.ratThree) {
              model.ratThree = false;
            } else {
              model.ratThree = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.ratThree ? 'images/selected_checkbox.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              RatingBar(
                initialRating: 3,
                minRating: 3,
                itemSize: ScreenUtil().setWidth(17),
                direction: Axis.horizontal,
                itemCount: 5,
                maxRating: 3,
                itemBuilder: (context, _) =>
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
              ),
              SizedBox(width: 5,),
              Text(
                '& Up',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        SizedBox(
          height: 15,
        ),
        InkWell(
          onTap: () {
            if (model.ratTwo) {
              model.literary = false;
            } else {
              model.ratTwo = true;
            }
          },
          child: Row(
            children: <Widget>[
              Image.asset(model.ratTwo ? 'images/selected_checkbox.png' : 'images/unselected_checkbox.png'),
              SizedBox(
                width: 10,
              ),
              RatingBar(
                initialRating: 2,
                minRating: 2,
                maxRating: 2,
                itemSize: ScreenUtil().setWidth(17),
                direction: Axis.horizontal,
                itemCount: 5,
                itemBuilder: (context, _) =>
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
              ),
              SizedBox(width: 5,),
              Text(
                '& Up',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(15), color: HexColor('#242126'), fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),

      ],
    );
  }
}
