import 'dart:async';

import 'package:book_store/core/viewmodels/splash_mView.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class SplashView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return SplashViewState();
  }
}

class SplashViewState extends State<SplashView>{
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375 , height: 852, allowFontScaling: false);
    return BaseView<SplashViewModel>(
      onModelReady: (model) {
        startSplashScreenTimer();
      },
      builder: (context, model, child) {
        return Scaffold(
          body: _body(),
        );
      },
    );
  }

  _body() {}

  startSplashScreenTimer() async {
    var duration = Duration(seconds: 1);
    return Timer(duration, navigationPage);
  }

  void navigationPage() async {
    Navigator.pushReplacementNamed(context, routeWelcome);
  }

}