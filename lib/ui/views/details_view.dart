import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/details_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class DetailsView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return DetailsViewState();
  }

}

class DetailsViewState extends State<DetailsView> {
  @override
  Widget build(BuildContext context) {

    return BaseView<DetailsViewModel>(
      onModelReady: (model){},

      builder: (context,model,child){
        return Scaffold(
          backgroundColor: HexColor('#EBE97C'),
          body: _body(),
          bottomNavigationBar: _bottomNavigationBar(),
        );
      },
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.all(15),
      child: ListView(
        physics: BouncingScrollPhysics(),
        shrinkWrap: true,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Row(
            children: <Widget>[
              InkWell(
                  onTap: (){
                    Navigator.pop(context);
                  },child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.arrow_back),
                  )),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: ScreenUtil().setWidth(120),
                margin: EdgeInsets.only(left: 20, right: 20),
                child: Stack(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Container(
                        height: ScreenUtil().setHeight(240),
                        width: ScreenUtil().setWidth(160),
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(.5)
                        ),
                        child: CachedNetworkImage(
                          imageUrl: "https://images-na.ssl-images-amazon.com/images/I/41pps%2BZIgOL._SX321_BO1,204,203,200_.jpg",
                          fit: BoxFit.fill,
                          placeholder: (context, url) =>
                              CommonLoader(
                                size: small,
                              ),
                          errorWidget: (context, url, error) =>
                              Icon(
                                Icons.broken_image,
                              ),
                        ),
                      ),
                    ),
                  ],

                ),
              )
            ],
          ),
          SizedBox(height: 20,),
          Text("My Thoughts Exactly", textAlign: TextAlign.center,
            style: TextStyle(color: HexColor("#242126"),
                fontSize: ScreenUtil().setSp(24),
                fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 10,),
          Text("by Lily Allen ", textAlign: TextAlign.center,
            style: TextStyle(color: HexColor("#242126"),
                fontSize: ScreenUtil().setSp(12),
                fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 15,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              RatingBar(
                initialRating: 4,
                minRating: 4,
                maxRating: 4,
                itemSize: ScreenUtil().setWidth(20),
                direction: Axis.horizontal,
                itemCount: 5,
                itemBuilder: (context, _) =>
                    Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
              ),
            ],
          ),
          SizedBox(height: 10,),
          Text("6,174 Reviews", textAlign: TextAlign.center,
            style: TextStyle(color: HexColor("#242126"),
                fontSize: ScreenUtil().setSp(12),
                fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset('images/left_leaf.png'),
              SizedBox(width: 5,),
              Text("AWARDS", textAlign: TextAlign.center,
                style: TextStyle(color: HexColor("#242126"),
                    fontSize: ScreenUtil().setSp(10),
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(width: 5,),
              Image.asset('images/right_leaf.png'),
            ],
          ),
          SizedBox(height: 20,),
          Text("Shortlisted for the",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: HexColor("#242126"),
                fontSize: ScreenUtil().setSp(12),
                fontWeight: FontWeight.bold
            ),),
          SizedBox(height: 5,),
          Text("Specsavers National Book Awards 2018",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: HexColor("#242126"),
                fontSize: ScreenUtil().setSp(12),
                fontWeight: FontWeight.bold
            ),),
          SizedBox(height: 15,),
          Row(
            children: <Widget>[
              Text("Customer Reviews", textAlign: TextAlign.center,
                style: TextStyle(color: HexColor("#242126"),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17)),
              ),
            ],
          ),
          SizedBox(height: 15,),
          viewCustomerReview(),
          SizedBox(height: 20,),

        ],
      ),
    );
  }

  _bottomNavigationBar() {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topRight: Radius.circular(15),topLeft: Radius.circular(15)),
          color: HexColor("#FFFFFF")),
      padding: EdgeInsets.only(top: 15,bottom: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(25)),
                color: HexColor("#00D6D8")),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('images/book_icon.png'),
                SizedBox(
                  width: 5,
                ),
                Text(
                  '\$15.60',
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.bold,
                      color: HexColor('#FFFFFF')),
                ),
              ],
            ),
            height: ScreenUtil().setHeight(56),
            width: ScreenUtil().setWidth(160),
          ),
          SizedBox(
            width: 8,
          ),
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(25)),
                color: HexColor("#EDEFF3")),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('images/music.png'),
                SizedBox(
                  width: 5,
                ),
                Text(
                  '\$19.08',
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.bold,
                      color: HexColor('#242126')),
                ),
              ],
            ),
            height: ScreenUtil().setHeight(56),
            width: ScreenUtil().setWidth(160),
          ),
        ],
      ),
    );

  }

  viewCustomerReview() {
   return Container(
      height: ScreenUtil().setHeight(130),
      child:  ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return Container(

              width:ScreenUtil().setWidth(315),
              height: ScreenUtil().setHeight(130),
              margin: EdgeInsets.only(right: 10),
              decoration: BoxDecoration(
                color: HexColor('#FAFAFA'),
                borderRadius: BorderRadius.all(Radius.circular(8)),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      RatingBar(
                        initialRating: 4,
                        minRating: 4,
                        maxRating: 4,
                        itemSize: ScreenUtil().setWidth(16),
                        direction: Axis.horizontal,
                        itemCount: 5,
                        itemBuilder: (context, _) =>
                            Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(5),),
                      Text(
                        'An unflinching,unputdownable book Source',
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(15), fontWeight: FontWeight.bold, color: HexColor('#242126')),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(5),),
                      Text(
                        'Guardian',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(12), fontWeight: FontWeight.bold, color: HexColor('#D1DDDF')),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}