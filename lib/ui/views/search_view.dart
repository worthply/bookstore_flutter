import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/search_view_model.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/views/search_category_view.dart';
import 'package:book_store/ui/views/search_chart_view.dart';
import 'package:book_store/ui/views/search_more_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SearchView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SearchViewState();
  }
}

class SearchViewState extends State<SearchView> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return BaseView<SearchViewModel>(
        onModelReady: (model) {
      model.tabController = new TabController(length: 3, vsync: this);
      model.tabController.addListener(() {
        model.tabController.index;
        model.notifyListeners();
      });
    }, builder: (context, model, child) {
      return Scaffold(
        body: _body(model),

      );
    });
  }

  _body(SearchViewModel model) {

    return Column(
      children: <Widget>[
        InkWell(
          onTap: (){
            Navigator.pushNamed(context, routeSearchResultView);
          },
          child: Container(
            margin: EdgeInsets.only(left: 20, right: 20, top: 35),
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor("#EDEFF3")),
            child: TextFormField(
              textInputAction: TextInputAction.next,
              enabled: false,
              maxLines: 1,
              style: TextStyle(
                color: Colors.black,
                fontSize: ScreenUtil().setSp(17),
              ),
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.search,color: Colors.black,),
                border: InputBorder.none,
                hintText: 'Search Books',
                hintStyle:
                    TextStyle(color: HexColor("#AFC1C4"), fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(17)),
              ),
            ),
          ),
        ),
        SizedBox(height: 30,),
        TabBar(
          controller: model.tabController,
          indicatorColor: Colors.transparent,
          isScrollable: false,
          labelColor: HexColor('#242126'),
          labelStyle: TextStyle(fontWeight: FontWeight.bold),
          unselectedLabelStyle: TextStyle(color: HexColor('#8f2b4f'),
              fontSize:ScreenUtil().setSp(22),
              fontWeight: FontWeight.bold
          ),
          unselectedLabelColor: HexColor("#D1DDDF"),
          tabs: [
            Tab(
              text: "Category",
            ),
            Tab(
              text: "Chart",
            ),
            Tab(
              text: "More in",
            ),
          ],
        ),
        Expanded(
          child: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: model.tabController,
            children: <Widget>[
              CategorySearchView(),
              ChartSearchView(),
              MoreSearchView(),
            ],
          ),
        ),
      ],
    );
  }
}
