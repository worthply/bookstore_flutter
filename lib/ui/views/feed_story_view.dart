import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/feed_story_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class FeedStoryView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return FeedStoryViewState ();
  }

}

class FeedStoryViewState extends State<FeedStoryView> {
  @override
  Widget build(BuildContext context) {

    return BaseView<FeedStoryViewMode>(
      builder:(context,model,child){
        return Scaffold(
          body: _body(),
        );

      } ,
    );
  }
  _body() {
    return Stack(
      children: <Widget>[
        Container(
          height: double.infinity,
          width: double.infinity,
          child: CachedNetworkImage(
            imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIXsc3UJfDcHP6aluRV7m0As_rmbqtZwJMGS9p-D6MZ7b-3UMCiQ&s",
            fit: BoxFit.fill,
            placeholder: (context, url) =>
                CommonLoader(
                  size: small,
                ),
            errorWidget: (context, url, error) =>
                Icon(
                  Icons.broken_image,
                ),
          ),
        ),
        Align(
          alignment: Alignment(-.9,-.8),
          child: Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 10),
                child: Container(
                  padding: EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Container(
                      height: ScreenUtil().setHeight(60),
                      width: ScreenUtil().setHeight(60),
                      child: CachedNetworkImage(
                        imageUrl: "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                        fit: BoxFit.fill,
                        placeholder: (context, url) =>
                            CommonLoader(
                              size: small,
                            ),
                        errorWidget: (context, url, error) =>
                            Icon(
                              Icons.broken_image,
                            ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Karin Hiselius',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: HexColor('#FFFFFF'),
                          fontSize: ScreenUtil().setSp(15),
                          fontWeight: FontWeight.bold

                      ),),
                    SizedBox(height: 5,),
                    Text('2 Hours ago',
                      style: TextStyle(
                          color: HexColor('#707070'),
                          fontSize: ScreenUtil().setSp(10),
                          fontWeight: FontWeight.bold

                      ),)
                  ],
                ),
              )
            ],
          ),
        ),
        Align(
          alignment: Alignment(0,.85),
          child: Container
            (
            margin: EdgeInsets.only(left: 35, right: 35,top: 15),
            padding: EdgeInsets.fromLTRB(10,5,10,5),
            decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)),
                color: HexColor("#EDEFF3")),
            child: TextFormField(
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.emailAddress,
              maxLines: 1,
              style: TextStyle(
                color: Colors.black,
                fontSize: ScreenUtil().setSp(17),
              ),
              decoration: InputDecoration(
                suffixIcon: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.asset("images/emoji.png"),
                    SizedBox(width: 10,),
                    Image.asset("images/send_icon.png")
                  ],
                ),
                border: InputBorder.none,
                hintText: 'Send Message',
                hintStyle: TextStyle(
                    color: HexColor("#707070"),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17)
                ),
              ),

            ),
          ),
        ),
      ],
    );

  }
}