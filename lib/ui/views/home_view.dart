import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/home_View_model.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeViewState();
  }
}

class HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<HomeViewModel>(
      onModelReady: (model) {},
      builder: (context, model, child) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: _body(),
        );
      },
    );
  }

  _body() {
    return ListView(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.all(0),
      children: <Widget>[
        Stack(
          children: <Widget>[
            Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height / 2.5,
              color: HexColor("#E6E367"),
            ),
            ListView(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,
              children: <Widget>[
                SizedBox(height: 20,),
                Text("My Thoughts Exactly", textAlign: TextAlign.center,
                  style: TextStyle(color: HexColor("#242126"),
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 2,
                      width: 50,
                      decoration: BoxDecoration(
                          color: HexColor("#242126")
                      ),),
                  ],
                ),
                SizedBox(height: 20,),
                Text("'An unflinching,\nunputdownable book'",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: HexColor("#242126"),
                      fontSize: ScreenUtil().setSp(24),
                      fontWeight: FontWeight.bold
                  ),),
                SizedBox(height: 10,),
                Text("Guardian", textAlign: TextAlign.center,
                  style: TextStyle(color: HexColor("#707070"),
                      fontSize: ScreenUtil().setSp(16)),
                ),
                SizedBox(height: 20,),
                Container(
                  height: 180,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: 5,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: (){
                            Navigator.pushNamed(context, routeDetailsView);
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 20, right: 20),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Container(
                                height: ScreenUtil().setHeight(240),
                                width: ScreenUtil().setWidth(160),
                                decoration: BoxDecoration(
                                  color: Colors.grey.withOpacity(.5)
                                ),
                                child: CachedNetworkImage(
                                  imageUrl: "https://images-na.ssl-images-amazon.com/images/I/51xRQjtghDL._SX322_BO1,204,203,200_.jpg",
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) =>
                                      CommonLoader(
                                        size: small,
                                      ),
                                  errorWidget: (context, url, error) =>
                                      Icon(
                                        Icons.broken_image,
                                      ),
                                ),
                              ),
                            ),
                          ),
                        );
                      }
                  ),
                ),
                SizedBox(height: 20,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text("Story", style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),
                SizedBox(height: 20,),
                storyView(),
                SizedBox(height: 20,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text("Bestselling", style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),
                SizedBox(height: 20,),
                bestSellingView(),
                SizedBox(height: 20,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text("Series", style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),
                SizedBox(height: 20,),
                seriesView(),
                SizedBox(height: 20,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text("A friend is reading", style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),
                SizedBox(height: 20,),
                friendReadingView(),
                SizedBox(height: 20,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text("Audio Books", style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),
                SizedBox(height: 20,),
                audioBookView(),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text("You may also like", style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),
                SizedBox(height: 20,),
                alsoLikeView(),
                SizedBox(height: 20,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text("New Releases", style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),
                SizedBox(height: 20,),
                newReleasesView(),
                SizedBox(height: 20,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text("Coming Soon", style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.bold
                    ),),
                  ],
                ),
                SizedBox(height: 10,),
                Row(
                  children: <Widget>[
                    SizedBox(width: 20,),
                    Text("Week of April 8", style: TextStyle(
                        fontSize: ScreenUtil().setSp(24),
                        fontWeight: FontWeight.bold,
                        color: HexColor('#AFC1C4')
                    ),),
                  ],
                ),
                SizedBox(height: 10,),
                comingSoonView()

              ],
            ),

          ],
        ),
      ],

    );
  }

  storyView() {
    return Container(
      height: ScreenUtil().setHeight(220),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              width: ScreenUtil().setWidth(120),
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Stack(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Container(
                      height: ScreenUtil().setHeight(200),
                      width: ScreenUtil().setWidth(120),
                      decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(.5)
                      ),
                      child: CachedNetworkImage(
                        imageUrl: "https://images-na.ssl-images-amazon.com/images/I/41pps%2BZIgOL._SX321_BO1,204,203,200_.jpg",
                        fit: BoxFit.fill,
                        placeholder: (context, url) =>
                            CommonLoader(
                              size: small,
                            ),
                        errorWidget: (context, url, error) =>
                            Icon(
                              Icons.broken_image,
                            ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment(1, -.5),
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        "I’ve missed you, too.” Though he was sheepish, I thought I saw something else in his eyes. After",
                        style: TextStyle(
                            color: HexColor("#FFFFFF"),
                            fontSize: ScreenUtil().setSp(15),
                            fontWeight: FontWeight.bold
                        ),),
                    ),
                  ),
                  Align(
                    alignment: Alignment(0, 1),
                    child: Container(
                      decoration: BoxDecoration(),
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Container(
                          height: ScreenUtil().setHeight(60),
                          width: ScreenUtil().setHeight(60),
                          decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(.5)
                          ),
                          child: CachedNetworkImage(
                            imageUrl: "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                            fit: BoxFit.fill,
                            placeholder: (context, url) =>
                                CommonLoader(
                                  size: small,
                                ),
                            errorWidget: (context, url, error) =>
                                Icon(
                                  Icons.broken_image,
                                ),
                          ),
                        ),
                      ),
                    ),
                  ),

                ],

              ),
            );
          }
      ),
    );
  }

  bestSellingView() {
    return Container(
      height: ScreenUtil().setHeight(346),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              width: ScreenUtil().setWidth(120),
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Container(
                      height: ScreenUtil().setHeight(200),
                      width: ScreenUtil().setWidth(120),
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(.5)
                      ),
                      child: CachedNetworkImage(
                        imageUrl: "https://images-na.ssl-images-amazon.com/images/I/813TSQsp2wL._AC_UL320_SR220,320_.jpg",
                        fit: BoxFit.fill,
                        placeholder: (context, url) =>
                            CommonLoader(
                              size: small,
                            ),
                        errorWidget: (context, url, error) =>
                            Icon(
                              Icons.broken_image,
                            ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Text("The Case for Trump",
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(15),
                        fontWeight: FontWeight.bold,
                        color: HexColor("#242126")

                    ),),
                  SizedBox(height: 10,),
                  RatingBar(
                    initialRating: 3,
                    minRating: 1,
                    itemSize: ScreenUtil().setWidth(17),
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,

                    itemBuilder: (context, _) =>
                        Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  SizedBox(height: 5),
                  Text("6 Reviews",
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        fontWeight: FontWeight.bold,
                        color: HexColor("#D1DDDF")

                    ),),
                ],
              ),
            );
          }
      ),
    );
  }

  friendReadingView() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      height: ScreenUtil().setHeight(240),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.only(right: 7,left: 7),
              child: Card(
                elevation: 3,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Container(
                    width: ScreenUtil().setWidth(320),
                    height: ScreenUtil().setWidth(160),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: HexColor("#41A6B2")
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: Container(
                                  height: ScreenUtil().setHeight(120),
                                  width: ScreenUtil().setWidth(80),
                                  decoration: BoxDecoration(
                                      color: Colors.grey.withOpacity(.5)
                                  ),
                                  child: CachedNetworkImage(
                                    imageUrl: "https://images-na.ssl-images-amazon.com/images/I/813TSQsp2wL._AC_UL320_SR220,320_.jpg",
                                    fit: BoxFit.fill,
                                    placeholder: (context, url) =>
                                        CommonLoader(
                                          size: small,
                                        ),
                                    errorWidget: (context, url, error) =>
                                        Icon(
                                          Icons.broken_image,
                                        ),
                                  ),
                                ),
                              ),
                              SizedBox(width: 5,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Text("The Second Mountain",
                                    maxLines: 2,
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(15),
                                        fontWeight: FontWeight.bold,
                                        color: HexColor("#242126")

                                    ),),
                                  SizedBox(height: 5,),
                                  Text("by David Brooks",

                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(12),
                                        fontWeight: FontWeight.bold,
                                        color: HexColor("#707070").withOpacity(.5)

                                    ),),
                                  SizedBox(height: 20,),
                                  RatingBar(
                                    initialRating: 3,
                                    minRating: 1,
                                    itemSize: ScreenUtil().setWidth(17),
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,

                                    itemBuilder: (context, _) =>
                                        Icon(
                                          Icons.star,
                                          color: Colors.amber,
                                        ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                  SizedBox(height: 5),
                                  Text("122 Reviews",
                                    style: TextStyle(
                                        fontSize: ScreenUtil().setSp(12),
                                        fontWeight: FontWeight.bold,
                                        color: HexColor("#D1DDDF")

                                    ),),
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 20,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("16 friends reads",
                            style: TextStyle(
                              color: HexColor("#AFC1C4"),
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.bold
                            ),),
                            Expanded(
                              child: Container(
                                child: Stack(
                                  children: <Widget>[
                                    Align(
                                      alignment: Alignment(-.6, 0),
                                      child: Container(
                                        decoration: BoxDecoration(),
                                        margin: EdgeInsets.only(left: 20, right: 20),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(100),
                                          child: Container(
                                            height: ScreenUtil().setHeight(50),
                                            width: ScreenUtil().setHeight(50),
                                            decoration: BoxDecoration(
                                                color: Colors.grey.withOpacity(.5)
                                            ),
                                            child: CachedNetworkImage(
                                              imageUrl: "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                                              fit: BoxFit.fill,
                                              placeholder: (context, url) =>
                                                  CommonLoader(
                                                    size: small,
                                                  ),
                                              errorWidget: (context, url, error) =>
                                                  Icon(
                                                    Icons.broken_image,
                                                  ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(-.2, 0),
                                      child: Container(
                                        decoration: BoxDecoration(),
                                        margin: EdgeInsets.only(left: 20, right: 20),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(100),
                                          child: Container(
                                            height: ScreenUtil().setHeight(50),
                                            width: ScreenUtil().setHeight(50),
                                            decoration: BoxDecoration(
                                                color: Colors.grey.withOpacity(.5)
                                            ),
                                            child: CachedNetworkImage(
                                              imageUrl: "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                                              fit: BoxFit.fill,
                                              placeholder: (context, url) =>
                                                  CommonLoader(
                                                    size: small,
                                                  ),
                                              errorWidget: (context, url, error) =>
                                                  Icon(
                                                    Icons.broken_image,
                                                  ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(.2, 0),
                                      child: Container(
                                        decoration: BoxDecoration(),
                                        margin: EdgeInsets.only(left: 20, right: 20),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(100),
                                          child: Container(
                                            height: ScreenUtil().setHeight(50),
                                            width: ScreenUtil().setHeight(50),
                                            decoration: BoxDecoration(
                                                color: Colors.grey.withOpacity(.5)
                                            ),
                                            child: CachedNetworkImage(
                                              imageUrl: "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                                              fit: BoxFit.fill,
                                              placeholder: (context, url) =>
                                                  CommonLoader(
                                                    size: small,
                                                  ),
                                              errorWidget: (context, url, error) =>
                                                  Icon(
                                                    Icons.broken_image,
                                                  ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(.6, 0),
                                      child: Container(
                                        decoration: BoxDecoration(),
                                        margin: EdgeInsets.only(left: 20, right: 20),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(100),
                                          child: Container(
                                            height: ScreenUtil().setHeight(50),
                                            width: ScreenUtil().setHeight(50),
                                            decoration: BoxDecoration(
                                                color: Colors.grey.withOpacity(.5)
                                            ),
                                            child: CachedNetworkImage(
                                              imageUrl: "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                                              fit: BoxFit.fill,
                                              placeholder: (context, url) =>
                                                  CommonLoader(
                                                    size: small,
                                                  ),
                                              errorWidget: (context, url, error) =>
                                                  Icon(
                                                    Icons.broken_image,
                                                  ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(1, 0),
                                      child: Container(
                                        decoration: BoxDecoration(),
                                        margin: EdgeInsets.only(left: 20, right: 20),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(100),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: HexColor("#707070")
                                            ),
                                            height: ScreenUtil().setHeight(50),
                                            width: ScreenUtil().setHeight(50),
                                            child: Center(child: Text("+12",style: TextStyle(
                                              color: HexColor("#FFFFFF"),
                                              fontWeight: FontWeight.bold,
                                              fontSize: ScreenUtil().setSp(12)
                                            ),)),
                                          ),
                                        ),
                                      ),
                                    ),


                                  ],
                                ),
                              ),
                            )

                          ],
                        )

                      ],
                    ),
                  ),
                ),
              ),
            );
          }
      ),
    );
  }

  audioBookView() {
    return Container(
      height: ScreenUtil().setHeight(326),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height:ScreenUtil().setWidth(120),
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8),
                    child: Container(
                      height: ScreenUtil().setHeight(180),
                      width: ScreenUtil().setWidth(180),
                      decoration: BoxDecoration(
                        color: HexColor('#DBD125')
                      ),
                      child: Stack(
                        children: <Widget>[
                          Center(
                            child: Container(
                              height: ScreenUtil().setHeight(120),
                              width: ScreenUtil().setWidth(80),
                              decoration: BoxDecoration(
                                  color: Colors.grey.withOpacity(.5)
                              ),
                              child: CachedNetworkImage(
                                imageUrl: "https://images-na.ssl-images-amazon.com/images/I/813TSQsp2wL._AC_UL320_SR220,320_.jpg",
                                fit: BoxFit.fill,
                                placeholder: (context, url) =>
                                    CommonLoader(
                                      size: small,
                                    ),
                                errorWidget: (context, url, error) =>
                                    Icon(
                                      Icons.broken_image,
                                    ),
                              ),
                            ),
                          ),
                          Align(
                              alignment: Alignment(0.6,.8),
                              child: Image.asset("images/earphone.png"))
                        ],

                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Text("The Case for Trump",
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(15),
                        fontWeight: FontWeight.bold,
                        color: HexColor("#242126")

                    ),),
                  SizedBox(height: 10,),
                  RatingBar(
                    initialRating: 3,
                    minRating: 1,
                    itemSize: ScreenUtil().setWidth(17),
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,

                    itemBuilder: (context, _) =>
                        Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  SizedBox(height: 5),
                  Text("6 Reviews",
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        fontWeight: FontWeight.bold,
                        color: HexColor("#D1DDDF")

                    ),),
                ],
              ),
            );
          }
      ),
    );
  }

  alsoLikeView() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      height: ScreenUtil().setHeight(160),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.only(right: 7,left: 7),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Container(
                  width: ScreenUtil().setWidth(320),
                  height: ScreenUtil().setWidth(160),
                  decoration: BoxDecoration(
                      color: HexColor("#B71B66")
                  ),
                  child: Center(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Container(
                            margin: EdgeInsets.only(left: 5),
                            height: ScreenUtil().setHeight(120),
                            width: ScreenUtil().setWidth(80),
                            decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(.5)
                            ),
                            child: CachedNetworkImage(
                              imageUrl: "https://images-na.ssl-images-amazon.com/images/I/813TSQsp2wL._AC_UL320_SR220,320_.jpg",
                              fit: BoxFit.fill,
                              placeholder: (context, url) =>
                                  CommonLoader(
                                    size: small,
                                  ),
                              errorWidget: (context, url, error) =>
                                  Icon(
                                    Icons.broken_image,
                                  ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                                Text("Dinner Party: A laugh out loud...",
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(15),
                                    fontWeight: FontWeight.bold,
                                    color: HexColor("#FFFFFF"),

                                ),),
                              SizedBox(height: 5,),
                              Text("by David Brooks",

                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(12),
                                    fontWeight: FontWeight.bold,
                                    color: HexColor("#707070").withOpacity(.5)

                                ),),
                              SizedBox(height: 20,),
                              RatingBar(
                                initialRating: 3,
                                minRating: 1,
                                itemSize: ScreenUtil().setWidth(17),
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,

                                itemBuilder: (context, _) =>
                                    Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                              SizedBox(height: 5),
                              Text("122 Reviews",
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(12),
                                    fontWeight: FontWeight.bold,
                                    color: HexColor("#D1DDDF")

                                ),),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
      ),
    );
  }

  newReleasesView() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      height: ScreenUtil().setHeight(150),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              height: ScreenUtil().setHeight(172),
              child: Container(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Container(
                        height: ScreenUtil().setHeight(120),
                        width: ScreenUtil().setWidth(80),
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(.5)
                        ),
                        child: CachedNetworkImage(
                          imageUrl: "https://images-na.ssl-images-amazon.com/images/I/813TSQsp2wL._AC_UL320_SR220,320_.jpg",
                          fit: BoxFit.fill,
                          placeholder: (context, url) =>
                              CommonLoader(
                                size: small,
                              ),
                          errorWidget: (context, url, error) =>
                              Icon(
                                Icons.broken_image,
                              ),
                        ),
                      ),
                    ),
                    SizedBox(width: 5,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(.5),
                            borderRadius: BorderRadius.all(Radius.circular(20))
                          ),
                          child: Text('Noble',style: TextStyle(
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.bold,
                            color: HexColor("#433338"),
                          ),),
                        ),
                        SizedBox(height: 20,),
                        Text("The Second Mountain",
                          maxLines: 2,
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(15),
                              fontWeight: FontWeight.bold,
                              color: HexColor("#242126")

                          ),),
                        SizedBox(height: 5,),
                        Text("by David Brooks",
                          style: TextStyle(
                              fontSize: ScreenUtil().setSp(12),
                              fontWeight: FontWeight.bold,
                              color: HexColor("#707070").withOpacity(.5)

                          ),),
                      ],
                    )
                  ],
                ),
              ),
            );
          }
      ),
    );

  }

  seriesView() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      height: ScreenUtil().setHeight(250),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.only(right: 10,left: 10),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: Container(
                  height: ScreenUtil().setHeight(250),
                  width: ScreenUtil().setWidth(320),
                  decoration: BoxDecoration(
                    color: HexColor('#817CB7')
                  ),
                  padding: EdgeInsets.all(15),
                  child: Column(
                    children: <Widget>[
                      Text('The Selection 4-Book Box Set:The Complete Series',
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: HexColor('#FFFFFF'),
                        fontWeight: FontWeight.bold,
                        fontSize: ScreenUtil().setSp(15)
                      ),),
                      SizedBox(height: 10,),
                      Image.asset("images/bookSet.png",
                      width: ScreenUtil().setWidth(320),
                      height: ScreenUtil().setHeight(150) ,
                      fit: BoxFit.fill,)
                    ],
                  ),

                ),
              ),
            );
          }
      ),
    );

  }

  comingSoonView(){
    return Container(
      height: 130,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 5,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.only(left: 8, right: 8),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Container(
                  height: ScreenUtil().setHeight(120),
                  width: ScreenUtil().setWidth(90),
                  decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(.5)
                  ),
                  child: CachedNetworkImage(
                    imageUrl: "https://images-na.ssl-images-amazon.com/images/I/51xRQjtghDL._SX322_BO1,204,203,200_.jpg",
                    fit: BoxFit.fill,
                    placeholder: (context, url) =>
                        CommonLoader(
                          size: small,
                        ),
                    errorWidget: (context, url, error) =>
                        Icon(
                          Icons.broken_image,
                        ),
                  ),
                ),
              ),
            );
          }
      ),
    );
  }




}