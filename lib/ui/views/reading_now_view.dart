import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/reading_now_view_model.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
class ReadingNowView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return ReadingNowViewState();
  }
}

class ReadingNowViewState extends State<ReadingNowView> {
  @override
  Widget build(BuildContext context) {

    return BaseView<ReadingNowViewModel>(
      onModelReady: (model){

      },
      builder: (context,model,child){
        return Scaffold(
          body: _body(),
        );
      },
    );
  }

  _body() {
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.all(8),
        physics: BouncingScrollPhysics(),
        itemCount: 10,
        itemBuilder: (BuildContext context, int index){
          return InkWell(
            onTap: (){
              Navigator.pushNamed(context, routeReadView);
            },
            child: Container(
              width: ScreenUtil().setHeight(200),
              margin: EdgeInsets.only(right: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Container(
                      height: ScreenUtil().setHeight(300),
                      width: ScreenUtil().setHeight(200),
                      decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(.5)
                      ),
                      child: CachedNetworkImage(
                        imageUrl: "https://rukminim1.flixcart.com/image/416/416/jx9aefk0/book/2/9/3/all-in-one-science-cbse-class-10-original-imafhks5zmunfzbd.jpeg?q=70",
                        fit: BoxFit.fill,
                        placeholder: (context, url) =>
                            CommonLoader(
                              size: small,
                            ),
                        errorWidget: (context, url, error) =>
                            Icon(
                              Icons.broken_image,
                            ),
                      ),
                    ),
                  ),
                  SizedBox(height: 7,),
                  Text('The Unwinding of the The Unwinding of the The Unwinding of the …The Unwinding of the …The Unwinding of the ……',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(15),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                  ),),
                  SizedBox(height: 7,),
                  Text('by Camille Pagán',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(12),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#D1DDDF')
                  ),),
                  SizedBox(height: 7,),
                  Text('24%',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(12),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                  ),),
                  SizedBox(height: 7,),
                  new LinearPercentIndicator(
                    animation: true,
                    padding: EdgeInsets.only(left: 5,right: 5),
                    lineHeight: ScreenUtil().setHeight(10),
                    animationDuration: 2000,
                    percent: 0.24,
                    progressColor: HexColor('#52BC8B'),
                  ),
                ],
              ),
            ),
          );
        });
  }
}