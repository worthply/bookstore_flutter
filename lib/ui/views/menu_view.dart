import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/content_tab_view_model.dart';
import 'package:book_store/core/viewmodels/menu_view_model.dart';
import 'package:book_store/core/viewmodels/read_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/views/content_tab_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class MenuView extends StatefulWidget{
  ReadViewModel readViewModel;

  MenuView(ReadViewModel readViewModel){
    this.readViewModel=readViewModel;
  }

  @override
  State<StatefulWidget> createState() {

    return MenuViewState();
  }

}

class MenuViewState extends State<MenuView> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {

    return BaseView<MenuViewModel>(
      onModelReady: (model){
        model.tabController = new TabController(length: 2, vsync: this);
        model.tabController.addListener(() {
          model.tabController.index;
          model.notifyListeners();
        });
      },
      builder:(context,model,child){
        return Scaffold(
          body: _body(model),
        );
      } ,
    );
  }

  _body(MenuViewModel model) {
    return Column(
      children: <Widget>[
        SizedBox(height: 30,),
        TabBar(
          controller: model.tabController,
          indicatorColor: Colors.transparent,
          isScrollable: false,
          labelColor: HexColor('#242126'),
          labelStyle: TextStyle(fontWeight: FontWeight.bold),
          unselectedLabelStyle: TextStyle(color: HexColor('#8f2b4f'),
              fontSize:ScreenUtil().setSp(22),
              fontWeight: FontWeight.bold
          ),
          unselectedLabelColor: HexColor("#D1DDDF"),
          tabs: [
            Tab(
              text: "Contents",
            ),
            Tab(
              text: "Bookmark",
            ),

          ],
        ),
        Expanded(
          child: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: model.tabController,
            children: <Widget>[
              ContentTabView(),
              ContentTabView()
            ],
          ),
        ),
      ],
    );
  }


}