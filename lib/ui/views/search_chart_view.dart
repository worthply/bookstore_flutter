import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/search_charts_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/views/search_author_view.dart';
import 'package:book_store/ui/views/search_books_view.dart';
import 'package:book_store/ui/views/search_keyword_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ChartSearchView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return ChartSearchViewState();
  }
}

class ChartSearchViewState extends State<ChartSearchView> with SingleTickerProviderStateMixin{
  @override
  Widget build(BuildContext context) {

    return BaseView<ChartSearchViewModel>(
      onModelReady: (model){
        model.tabController = new TabController(length: 3, vsync: this);
        model.tabController.addListener(() {
          model.tabController.index;
          model.notifyListeners();
        });
      },
      builder: (context,model,children){
        return Scaffold(
          body: _body(model),
        );
      },
    );
  }

  _body(ChartSearchViewModel model) {
    return Column(
      children: <Widget>[
        SizedBox(height: 10,),
        TabBar(
          controller: model.tabController,
          labelPadding: EdgeInsets.all(0),
          indicatorColor: Colors.black,
          labelColor: HexColor('#242126'),
          labelStyle: TextStyle(fontWeight: FontWeight.bold),
          unselectedLabelStyle: TextStyle(color: HexColor('#8f2b4f'),
              fontSize:ScreenUtil().setSp(17),
              fontWeight: FontWeight.bold
          ),
          unselectedLabelColor: HexColor("#D1DDDF"),
          tabs: [
            Tab(
              text: "Books",
            ),
            Tab(
              text: "Authors",
            ),
            Tab(
              text: "Keywords",
            ),
          ],
        ),
        Expanded(
          child: TabBarView(
            controller: model.tabController,
            children: <Widget>[
              SearchBookView(),
              SearchAuthorView(),
              SearchKeywordView(),
            ],
          ),
        )
      ],
    );
  }
}