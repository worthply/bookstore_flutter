import 'package:book_store/core/viewmodels/search_keyword_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:flutter/material.dart';

class SearchKeywordView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return SearchKeywordViewState();
  }
}

class SearchKeywordViewState  extends State<SearchKeywordView>{
  @override
  Widget build(BuildContext context) {

    return BaseView<SearchKeywordViewModel>(
      builder: (context,model,child){
        return Scaffold(
          body: _body(),
        );
      },
    );
  }

  _body() {}
}