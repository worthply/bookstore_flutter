import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/bookshelf_view_model.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class BookShelfView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return BookShelfViewState();
  }
}

class BookShelfViewState extends State<BookShelfView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<BookShelfViewMode>(
      onModelReady: (model) {},
      builder: (context, model, child) {
        return Scaffold(
          body: _body(),
        );
      },
    );
  }

  _body() {
    return ListView(
      padding: EdgeInsets.all(8),
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text('Arts & Photography Books',style: TextStyle(
              color: HexColor('#242126'),
              fontWeight: FontWeight.bold,
              fontSize: ScreenUtil().setSp(17),
            ),),
            Text('2',style: TextStyle(
              color: HexColor('#00D6D8'),
              fontWeight: FontWeight.bold,
              fontSize: ScreenUtil().setSp(17),
            ),),
          ],
        ),
        SizedBox(height: 10,),
        artAndPhotographyView(),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Business & Money',style: TextStyle(
              color: HexColor('#242126'),
              fontWeight: FontWeight.bold,
              fontSize: ScreenUtil().setSp(17),
            ),),
            Text('10',style: TextStyle(
              color: HexColor('#00D6D8'),
              fontWeight: FontWeight.bold,
              fontSize: ScreenUtil().setSp(17),
            ),),

          ],
        ),
        SizedBox(height: 10,),
        businessAndMoney(),

      ],
    );
  }
  artAndPhotographyView() {
    return Container(
      height: ScreenUtil().setHeight(330),
      child:  ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: (){
                Navigator.pushNamed(context, routeAudioView);
              },
              child: Container(
                width: ScreenUtil().setHeight(120),
                margin: EdgeInsets.only(right: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Container(
                        height: ScreenUtil().setHeight(200),
                        width: ScreenUtil().setHeight(150),
                        decoration: BoxDecoration(color: Colors.grey.withOpacity(.5)),
                        child: CachedNetworkImage(
                          imageUrl:
                          "https://rukminim1.flixcart.com/image/416/416/jy7kyvk0/book/7/7/8/prep-guide-to-bitsat-2020-original-imafg76fqfm6nbep.jpeg?q=70",
                          fit: BoxFit.fill,
                          placeholder: (context, url) => CommonLoader(
                            size: small,
                          ),
                          errorWidget: (context, url, error) => Icon(
                            Icons.broken_image,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 7,
                    ),
                    Text(
                      'Good Days Start With Gratitude',
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(15), fontWeight: FontWeight.bold, color: HexColor('#242126')),
                    ),
                    SizedBox(
                      height: 7,
                    ),
                    Text(
                      'by Pretty Simple...',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: ScreenUtil().setSp(12), fontWeight: FontWeight.bold, color: HexColor('#D1DDDF')),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }

  businessAndMoney() {
    return Container(
      height: ScreenUtil().setHeight(330),
      child:  ListView.builder(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          itemCount: 10,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              width: ScreenUtil().setHeight(120),
              margin: EdgeInsets.only(right: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Container(
                      height: ScreenUtil().setHeight(200),
                      width: ScreenUtil().setHeight(150),
                      decoration: BoxDecoration(color: Colors.grey.withOpacity(.5)),
                      child: CachedNetworkImage(
                        imageUrl:
                        "https://rukminim1.flixcart.com/image/416/416/jx9aefk0/book/1/4/9/all-in-one-social-science-cbse-class-10-original-imafhrhczpckgtcu.jpeg?q=70",
                        fit: BoxFit.fill,
                        placeholder: (context, url) => CommonLoader(
                          size: small,
                        ),
                        errorWidget: (context, url, error) => Icon(
                          Icons.broken_image,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Text(
                    'Good Days Start With Gratitude',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(15), fontWeight: FontWeight.bold, color: HexColor('#242126')),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Text(
                    'by Pretty Simple...',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: ScreenUtil().setSp(12), fontWeight: FontWeight.bold, color: HexColor('#D1DDDF')),
                  ),
                ],
              ),
            );
          }),
    );
  }


}
