import 'package:book_store/core/viewmodels/search_more_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:flutter/material.dart';
class MoreSearchView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return MoreSearchViewState ();
  }
}

class MoreSearchViewState extends State<MoreSearchView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<MoreSearchViewModel>(
      builder: (context,model,child){
        return Scaffold(
          body: _body(),
        );
      },

    );
  }

  _body() {}
}