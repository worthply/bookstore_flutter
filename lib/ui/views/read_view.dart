import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/read_view_model.dart';
import 'package:book_store/ui/views/menu_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'base_view.dart';

class ReadView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ReadViewState();
  }
}

class ReadViewState extends State<ReadView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<ReadViewModel>(
      onModelReady: (model) {
        model.imageList.insert(0, null);
      },
      builder: (context, model, child) {
        return Scaffold(
          body: _body(model),
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            centerTitle: true,
            leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                )),
            title: Text(
              'The Unwinding of the Miracle',
              style: TextStyle(color: HexColor('#000000'),
              fontSize: ScreenUtil().setSp(17),
              fontWeight: FontWeight.bold),

            ),
            actions: <Widget>[
              Image.asset('images/menu.png')
            ],
          ),
        );
      },
    );
  }

  _body(ReadViewModel model) {
    return SafeArea(
      child: Stack(
         children: <Widget>[
           Container(
             padding: EdgeInsets.all(10),
             child: Column(
               children: <Widget>[
                 InkWell(
                   onTap: () {
                     Clipboard.setData(new ClipboardData(text: 'Hellllo'));
                     Scaffold.of(context).showSnackBar(SnackBar(content: Text('text copied')));
                   },
                   child: new SelectableText('Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello',
                   ),
                 ),
               ],
             ),

           ),
           colorSettingView(model),
         ],
      ),
    );
  }

  noteView(ReadViewModel model){
    return Container(
      height: MediaQuery.of(context).size.height-120,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(25),bottomLeft: Radius.circular(25)), color: HexColor('#FFFFFF'),

      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 30,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Note',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(24),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                ),),
              SizedBox(height: 15,),
              InkWell(
                onTap: (){
                  if(model.showNotes==false){
                    model.showNotes=true;
                  }
                  else{
                    model.showNotes=false;
                  }
                },
                child: Text('Cancel',
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.w100,
                      color: HexColor('#242126')
                  ),),
              ),
            ],
          ),
          Expanded(
            child: Container(
              child: ListView.builder(
                  itemCount: 5,
                  itemBuilder: (context,index){
                return Container(
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border(
                      left: BorderSide( //                   <--- left side
                        color: HexColor('#707070').withOpacity(.2),
                        width: 5.0,
                      ),
                    ),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                    ),
                    child: Text('Sometimes', style: TextStyle(
                      color: HexColor('#000000'),
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.bold
                    ),),
                  ),

                );
              }),
            ),
          ),
          InkWell(
            onTap: (){
              if(model.showNotes==false){
                model.showNotes=true;
              }
              else{
                model.showNotes=false;
              }
            },
            child: Container(
              width: double.infinity,
              height: 55,
              decoration:
              BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#00D6D8').withOpacity(.1)),
              child: Center(
                child: Text('Done',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ScreenUtil().setSp(17),
                      color: HexColor('#00D6D8')

                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  newPost(ReadViewModel model){
    return Container(
      height: MediaQuery.of(context).size.height-50,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(25),bottomLeft: Radius.circular(25)), color: HexColor('#FFFFFF'),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: ScreenUtil().setSp(30),),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('New Post',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(24),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                ),),
              SizedBox(height: 15,),
              InkWell(
                onTap: (){
                  if(model.showNotes==false){
                    model.showNotes=true;
                  }
                  else{
                    model.showNotes=false;
                  }
                },
                child: Text('Cancel',
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.w100,
                      color: HexColor('#242126')
                  ),),
              ),
            ],
          ),
          SizedBox(height: ScreenUtil().setSp(30),),
          Container(
            height: ScreenUtil().setHeight(335),
            width: ScreenUtil().setHeight(335),
            child: Stack(
              children: <Widget>[
                 ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child:  Container(
                      height: ScreenUtil().setHeight(335),
                      width: ScreenUtil().setHeight(335),
                      decoration: BoxDecoration(
                        color: Colors.grey,
                      ),
                      child: model.bigImage!=null?Image.file(model.bigImage,fit: BoxFit.fill,):Center(child: Container()),

                    ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Text('“Yes,” he said.',
                  style: TextStyle(
                    color: HexColor('#FFFFFF'),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17)
                  ),
                  ),
                )


              ],
            ),
          ),
          SizedBox(height: ScreenUtil().setSp(30),),
          Container(
            height: ScreenUtil().setHeight(88),
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: model.imageList.length,
                physics: BouncingScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return InkWell(
                    onTap: (){
                      if(index==0){
                        showBottomSheet(context, model);  
                      }
                      else{
                        model.bigImage=model.imageList[index];
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Container(
                          height: ScreenUtil().setHeight(88),
                          width: ScreenUtil().setHeight(88),
                          decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(.5)
                          ),
                          child:model.imageList[index]!=null?Image.file(model.imageList[index],fit: BoxFit.fill,):Center(child: Image.asset('images/camera.png'),),
                        ),
                      ),
                    ),
                  );
                  
                }
            ),
          ),
          SizedBox(height: ScreenUtil().setSp(30),),
          InkWell(
            onTap: (){
              if(model.showNewPost==false){
                model.showNewPost=true;
              }
              else{
                model.showNewPost=false;
              }
            },
            child: Container(
              width: double.infinity,
              height: 55,
              decoration:
              BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#00D6D8').withOpacity(.1)),
              child: Center(
                child: Text('Next',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: ScreenUtil().setSp(17),
                      color: HexColor('#00D6D8')

                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  colorSettingView(ReadViewModel model){

    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(25),bottomLeft: Radius.circular(25)), color: HexColor('#FFFFFF')
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 30,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Color Setting',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(24),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                ),),
              SizedBox(height: 15,),
              InkWell(
                onTap: (){

                },
                child: Text('Cancel',
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(17),
                      fontWeight: FontWeight.w100,
                      color: HexColor('#242126')
                  ),),
              ),
            ],
          ),

          SizedBox(height: 15,),
          Container(
            width: double.infinity,
            height: 55,
            padding: EdgeInsets.only(left: 15,right: 15),
            decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: Colors.white,
                border: Border.all(
                    color: HexColor('#D1DDDF',)
                )),

            child: Center(
              child: DropdownButtonHideUnderline(
                child: DropdownButton(
                  isExpanded: true,
                  isDense: false,
                  hint: Text('Voice'),
                  style: TextStyle(
                    color: HexColor('#00D6D8'),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(15),
                  ),
                  value: model.voice,
                  onChanged: (value) {
                    setState(() {
                      model.voice = value;
                    });
                  },
                  items: [
                    DropdownMenuItem(
                      value: "0",
                      child: Text(
                        "Georgia",
                      ),
                    ),
                    DropdownMenuItem(
                      value: "1",
                      child: Text(
                        "Georgia",
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),

          SizedBox(height: 15,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Theme',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(17),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                ),),

            ],
          ),

          SizedBox(height: 15,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                    border: Border.all(
                        width: 2,
                        color: HexColor('#242126')
                    )
                ),
                child: Container(
                  height: ScreenUtil().setHeight(24),
                  width: ScreenUtil().setHeight(24),
                  decoration: BoxDecoration(
                    color: HexColor('#FFFFFF'),
                    shape: BoxShape.circle,

                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                    border: Border.all(
                        width: 2,
                        color: HexColor('#242126')
                    )
                ),
                child: Container(
                  height: ScreenUtil().setHeight(24),
                  width: ScreenUtil().setHeight(24),
                  decoration: BoxDecoration(
                    color: HexColor('#EDEFF3'),
                    shape: BoxShape.circle,

                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                    border: Border.all(
                        width: 2,
                        color: HexColor('#242126')
                    )
                ),
                child: Container(
                  height: ScreenUtil().setHeight(24),
                  width: ScreenUtil().setHeight(24),
                  decoration: BoxDecoration(
                    color: HexColor('#F2EFE9'),
                    shape: BoxShape.circle,

                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                    border: Border.all(
                        width: 2,
                        color: HexColor('#242126')
                    )
                ),
                child: Container(
                  height: ScreenUtil().setHeight(24),
                  width: ScreenUtil().setHeight(24),
                  decoration: BoxDecoration(
                    color: HexColor('#3A363C'),
                    shape: BoxShape.circle,

                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(3),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                    border: Border.all(
                        width: 2,
                        color: HexColor('#242126')
                    )
                ),
                child: Container(
                  height: ScreenUtil().setHeight(24),
                  width: ScreenUtil().setHeight(24),
                  decoration: BoxDecoration(
                    color: HexColor('#000000'),
                    shape: BoxShape.circle,

                  ),
                ),
              ),

            ],
          ),

          SizedBox(height: 15,),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Scrolling View',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(17),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                ),),

            ],
          ),

          SizedBox(height: 10,),
          Container(
            width: double.infinity,
            height: 55,
            decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#EDEFF3')),
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: (){
                      model.scrolling=true;
                    },
                    child: Container(
                      height: 55,
                      margin: EdgeInsets.all(2),
                      decoration:
                      BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: model.scrolling?HexColor('#FFFFFF'):Colors.transparent),
                      child: Center(
                        child: Text(
                          "On",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: model.scrolling?HexColor(
                                "#00D6D8",
                              ):HexColor('#707070'),
                              fontSize: ScreenUtil().setSp(15),
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: InkWell(
                    onTap: (){
                      model.scrolling =false;
                    },
                    child: Container(
                      margin: EdgeInsets.all(2),
                      height: 55,
                      decoration:
                      BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: model.scrolling==false?HexColor('#FFFFFF'):Colors.transparent),
                      child: Center(
                        child: Text(
                          "Off",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: model.scrolling==false?HexColor(
                                "#00D6D8",
                              ):HexColor('#707070'),
                              fontSize: ScreenUtil().setSp(17),
                              fontWeight: FontWeight.bold),

                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),

          SizedBox(height: 20,),
          Container(
            width: double.infinity,
            height: 55,
            decoration:
            BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#00D6D8').withOpacity(.1)),
            child: Center(
              child: Text('Done',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: ScreenUtil().setSp(17),
                color: HexColor('#00D6D8'),
              ),),
            ),
          ),

        ],
      ),
    );
  }

// Open bottom sheet to chose camera or Gallery
  showBottomSheet(BuildContext context, ReadViewModel model) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(
                        'Pick image from',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black, fontSize: 25),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.grey,
                  height: 2,
                ),
                ListTile(
                    leading: new Icon(Icons.camera_alt),
                    title: new Text('Camera'),
                    onTap: () {
                      model.openCamera();
                      Navigator.pop(context);
                    }),
                Divider(
                  color: Colors.grey,
                  height: 2,
                ),
                ListTile(
                    leading: new Icon(Icons.image),
                    title: new Text('Gallery'),
                    onTap: () {
                      model.openGallery();
                      Navigator.pop(context);
                    }),
                Divider(
                  color: Colors.grey,
                  height: 2,
                ),
              ],
            ),
          );
        });
  }


}


