import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/library_view_mode.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/views/bookshelf_view.dart';
import 'package:book_store/ui/views/my_post_view.dart';
import 'package:book_store/ui/views/reading_now_view.dart';
import 'package:book_store/ui/views/search_category_view.dart';
import 'package:book_store/ui/views/search_chart_view.dart';
import 'package:book_store/ui/views/search_more_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LibraryView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return LibraryViewState();
  }
}

class LibraryViewState  extends  State<LibraryView> with SingleTickerProviderStateMixin{
  @override
  Widget build(BuildContext context) {
    return BaseView<LibraryViewModel>(
      onModelReady: (model){
        model.tabController = new TabController(length: 3, vsync: this);
        model.tabController.addListener(() {
          model.tabController.index;
          model.notifyListeners();
        });
      },
      builder: (context,model,child){
        return Scaffold(
          body: _body(model),
        );
      },
    );
  }

  _body(LibraryViewModel model) {
    return Container(
      padding: EdgeInsets.all(10),
      child: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10),
            Text('Library',
              textAlign: TextAlign.start,
              style: TextStyle(
              fontWeight: FontWeight.bold,
              color: HexColor('#242126'),
              fontSize: ScreenUtil().setSp(44)
            ),),
            SizedBox(height: 20,),
            TabBar(
              controller: model.tabController,
              indicatorColor: Colors.transparent,
              isScrollable: true,
              labelColor: HexColor('#242126'),
              labelStyle: TextStyle(fontWeight: FontWeight.bold),
              unselectedLabelStyle: TextStyle(color: HexColor('#8f2b4f'),
                  fontSize:ScreenUtil().setSp(24),
                  fontWeight: FontWeight.bold
              ),
              unselectedLabelColor: HexColor("#D1DDDF"),
              tabs: [
                Tab(
                  text: "Reading Now",
                ),
                Tab(
                  text: "Bookshelf",
                ),
                Tab(
                  text: "My Post",
                ),
              ],
            ),
            Expanded(
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: model.tabController,
                children: <Widget>[
                  ReadingNowView(),
                  BookShelfView(),
                  MyPostView(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}