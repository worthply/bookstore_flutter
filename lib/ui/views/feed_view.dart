import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/feed_view_model.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class FeedView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {

    return FeedViewState ();
  }
}

class FeedViewState extends State<FeedView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<FeedViewModel>(
      onModelReady: (model) {},
      builder: (context, model, child) {
        return Scaffold(
          body: _body(model),
        );
      },
    ) ;
  }

  _body(FeedViewModel model) {
    return ListView(
      padding: EdgeInsets.all(8),
      children: <Widget>[
        SizedBox(height: 30,),
        Text("Feed",style: TextStyle(
          color: HexColor('#242126'),
          fontSize: ScreenUtil().setSp(44),
          fontWeight: FontWeight.bold
        ),),
        SizedBox(height: 30),

        Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 10),
              child: Container(
                padding: EdgeInsets.all(2),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: HexColor('#00D6D8'),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Container(
                    height: ScreenUtil().setHeight(60),
                    width: ScreenUtil().setHeight(60),
                    child: Center(child: Text("+",style: TextStyle(
                      color:Colors.white,
                      fontSize:ScreenUtil().setSp(35)
                    ),)),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                height: ScreenUtil().setHeight(80),
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 8,
                    physics: BouncingScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: EdgeInsets.only(right: 10),
                        child: InkWell(
                          onTap: (){
                            Navigator.pushNamed(context, routeStoryView);
                          },
                          child: Container(
                            padding: EdgeInsets.all(2),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                                border: Border.all(
                                    color: HexColor("#006CFF"),
                                  width: 2
                                )
                            ),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: Container(
                                height: ScreenUtil().setHeight(60),
                                width: ScreenUtil().setHeight(60),
                                child: CachedNetworkImage(
                                  imageUrl: "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) =>
                                      CommonLoader(
                                        size: small,
                                      ),
                                  errorWidget: (context, url, error) =>
                                      Icon(
                                        Icons.broken_image,
                                      ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      );
                    }
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 30,),
        ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Container(
            height: ScreenUtil().setHeight(532),
            width: ScreenUtil().setWidth(335),
            child: Stack(
              children: <Widget>[
                Container(
                  height: ScreenUtil().setHeight(335),
                  width: double.infinity,
                  child: CachedNetworkImage(
                    imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIXsc3UJfDcHP6aluRV7m0As_rmbqtZwJMGS9p-D6MZ7b-3UMCiQ&s",
                    fit: BoxFit.fill,
                    placeholder: (context, url) =>
                        CommonLoader(
                          size: small,
                        ),
                    errorWidget: (context, url, error) =>
                        Icon(
                          Icons.broken_image,
                        ),
                  ),

                ),
                Align(
                  alignment: Alignment(0,-.5),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20,right: 20),
                    child: Text('“I’ve missed you, too.” Though he was sheepish, I thought I saw something else in his eyes. After three years, he probably didn’t love me anymore. Yet as I stared deeper into the black pools of his pupils, I allowed myself to consider that maybe he did.',
                      style: TextStyle(
                          color: HexColor('#FFFFFF'),
                          fontSize: ScreenUtil().setSp(17)
                      ),),
                  ),
                ),
                Align(
                  alignment: Alignment(-.9,.4),
                  child: Container(
                    height: ScreenUtil().setHeight(120),
                    width:ScreenUtil().setWidth(80),
                    child: CachedNetworkImage(
                      imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSizTDzO-brUiftTqrOCO2cly7s-degjbE1RqYnnyIpN6GXqDqq&s",
                      fit: BoxFit.fill,
                      placeholder: (context, url) =>
                          CommonLoader(
                            size: small,
                          ),
                      errorWidget: (context, url, error) =>
                          Icon(
                            Icons.broken_image,
                          ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment(0,.4),
                  child: Text('The Case for Trump',
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(15),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#242126')
                  ),),
                ),
                Align(
                  alignment: Alignment(0,.5),
                  child: Text('by Victor Davis Ha…',
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(15),
                    fontWeight: FontWeight.bold,
                    color: HexColor('#D1DDDF')
                  ),),
                ),
                Align(
                  alignment: Alignment(0,1),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: Card(
                      elevation: 3,
                      child: Container(
                        padding: EdgeInsets.all(5),
                        height: ScreenUtil().setHeight(100),
                        width: ScreenUtil().setWidth(360),
                        child: Center(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height: ScreenUtil().setHeight(80),
                                width:ScreenUtil().setWidth(45),
                                child: CachedNetworkImage(
                                  imageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSizTDzO-brUiftTqrOCO2cly7s-degjbE1RqYnnyIpN6GXqDqq&s",
                                  fit: BoxFit.fill,
                                  placeholder: (context, url) =>
                                      CommonLoader(
                                        size: small,
                                      ),
                                  errorWidget: (context, url, error) =>
                                      Icon(
                                        Icons.broken_image,
                                      ),
                                ),
                              ),
                              SizedBox(width: 10,),
                              Expanded(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('The Unwinding of the Miracle: A Memoir of Life',
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: HexColor('#242126'),
                                          fontSize: ScreenUtil().setSp(13),
                                          fontWeight: FontWeight.bold

                                      ),),
                                    SizedBox(height: 5,),
                                    Text('24%',
                                      style: TextStyle(
                                          color: HexColor('#D1DDDF'),
                                          fontSize: ScreenUtil().setSp(12),
                                          fontWeight: FontWeight.bold

                                      ),)
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),

      ],
    );
  }


}