import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/search_result_view_model.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SearchResultView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SearchResultViewState();
  }
}

class SearchResultViewState extends State<SearchResultView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<SearchResultViewModel>(
      builder: (context, model, child) {
        return Scaffold(
          body: _body(model),
        );
      },
    );
  }

  _body(SearchResultViewModel model) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 50,
          ),
          Row(
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Container(
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor("#EDEFF3")),
                  child: TextField(
                    textInputAction: TextInputAction.next,
                    maxLines: 1,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: ScreenUtil().setSp(17),
                    ),
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.black,
                      ),
                      suffixIcon: InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, routeFilterView);
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Image.asset('images/filter.png'),
                          )),
                      border: InputBorder.none,
                      hintText: 'Search Books',
                      hintStyle: TextStyle(
                          color: HexColor("#AFC1C4"), fontWeight: FontWeight.bold, fontSize: ScreenUtil().setSp(17)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                flex: 1,
                child: Text(
                  'Cancel',
                  style: TextStyle(
                      fontSize: ScreenUtil().setSp(17), fontWeight: FontWeight.w500, color: HexColor('#242126')),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Over 90,000 results for ',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(12), fontWeight: FontWeight.bold, color: HexColor('#AFC1C4')),
              ),
              Text(
                '“books best sellers"',
                style: TextStyle(
                    fontSize: ScreenUtil().setSp(12), fontWeight: FontWeight.bold, color: HexColor('#242126')),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: Container(
              child: ListView.builder(
                  padding: EdgeInsets.fromLTRB(15, 10, 15, 0),
                  scrollDirection: Axis.vertical,
                  itemCount: 20,
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      margin: EdgeInsets.only(bottom: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: ScreenUtil().setHeight(180),
                            width: ScreenUtil().setWidth(120),
                            color: Colors.grey.withOpacity(.5),
                            child: CachedNetworkImage(
                              imageUrl:
                                  "https://images-na.ssl-images-amazon.com/images/I/61AWo6sS%2B-L._SX378_BO1,204,203,200_.jpg",
                              fit: BoxFit.fill,
                              placeholder: (context, url) => CommonLoader(
                                size: small,
                              ),
                              errorWidget: (context, url, error) => Icon(
                                Icons.broken_image,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Where the Crawdads Sing',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(15),
                                    fontWeight: FontWeight.bold,
                                    color: HexColor('#242126')),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'by Delia Owens ',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(12),
                                    fontWeight: FontWeight.bold,
                                    color: HexColor('#D1DDDF')),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              RatingBar(
                                initialRating: 3,
                                minRating: 1,
                                itemSize: ScreenUtil().setWidth(17),
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                '8,184 Reviews',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(12),
                                    fontWeight: FontWeight.bold,
                                    color: HexColor('#D1DDDF')),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Aug 14, 2018',
                                style: TextStyle(
                                    fontSize: ScreenUtil().setSp(12),
                                    fontWeight: FontWeight.bold,
                                    color: HexColor('#D1DDDF')),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(25)),
                                        color: HexColor("#00D6D8")),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Image.asset('images/book_icon.png'),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          '\$15.60',
                                          style: TextStyle(
                                              fontSize: ScreenUtil().setSp(12),
                                              fontWeight: FontWeight.bold,
                                              color: HexColor('#FFFFFF')),
                                        ),
                                      ],
                                    ),
                                    height: ScreenUtil().setHeight(32),
                                    width: ScreenUtil().setWidth(89),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(25)),
                                        color: HexColor("#EDEFF3")),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Image.asset('images/music.png'),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Text(
                                          '\$19.08',
                                          style: TextStyle(
                                              fontSize: ScreenUtil().setSp(12),
                                              fontWeight: FontWeight.bold,
                                              color: HexColor('#242126')),
                                        ),
                                      ],
                                    ),
                                    height: ScreenUtil().setHeight(32),
                                    width: ScreenUtil().setWidth(89),
                                  ),
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  }),
            ),
          )
        ],
      ),
    );
  }
}
