import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/signin_mView.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SignInView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignInViewState();
  }
}

class SignInViewState extends State<SignInView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<SignInModelView>(
      onModelReady: (model) {},
      builder: (context, model, child) {
        return Scaffold(
          bottomNavigationBar: _bottomView(),
          backgroundColor: HexColor("#FFFFFF"),
          body: _body(),
          appBar: AppBar(
            backgroundColor: HexColor("#FFFFFF"),
            elevation: 0,
            leading: InkWell(
              onTap: (){
                Navigator.pop(context);},
                child: Icon(Icons.arrow_back,color: Colors.black,)),
          ),
        );
      },
    );
  }

  _body() {
    return ListView(
      physics: BouncingScrollPhysics(),
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(15),
          child: Text("Sign In",
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: ScreenUtil().setSp(24)
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 35, right: 35,top: 10),
          padding: EdgeInsets.fromLTRB(10,5,10,5),
          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)),
              color: HexColor("#EDEFF3")),
          child: TextFormField(
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
                color: Colors.black,
                fontSize: ScreenUtil().setSp(17)
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Email',
              hintStyle: TextStyle(
                  color: HexColor("#AFC1C4"),
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil().setSp(17)
              ),
            ),

          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 35, right: 35,top: 15),
          padding: EdgeInsets.fromLTRB(10,5,10,5),
          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)),
              color: HexColor("#EDEFF3")),
          child: TextFormField(
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.emailAddress,
            obscureText: true,
            style: TextStyle(
              color: Colors.black,
              fontSize: ScreenUtil().setSp(17),

            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Password',
              hintStyle: TextStyle(
                  color: HexColor("#AFC1C4"),
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil().setSp(17)
              ),
            ),

          ),
        ),
        SizedBox(height: 15,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(width: 35,),
            Image.asset("images/unselected_checkbox.png"),
            SizedBox(width: 10,),
            Text("Remember Me",style: TextStyle(
                color: HexColor("#AFC1C4"),
                fontWeight: FontWeight.bold,
                fontSize: ScreenUtil().setSp(12)

            ),),
            Expanded(
              child: Container(),
            ),
            Text("Forgot your Password?",style: TextStyle(
                color: HexColor("#AFC1C4"),
                fontWeight: FontWeight.bold,
                fontSize: ScreenUtil().setSp(12)

            ),),
            SizedBox(width: 35,),
          ],
        ),
        SizedBox(height: 10,),
        Container(
          width: double.infinity,
          height: 55,
          margin: EdgeInsets.only(left: 30,right: 30),
          decoration:
          BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor('#00FDFF').withOpacity(.2)),
          child: Center(
            child: Text(
              "Sign In & Continue",
              textAlign: TextAlign.start,
              style: TextStyle(
                  color: HexColor(
                    "#00D6D8",
                  ),
                  fontSize: ScreenUtil().setSp(17)),
            ),
          ),
        ),
        SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("OR",
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(15),
                  color: HexColor("#AFC1C4")
              ),),
          ],
        ),
        SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("images/facebook.png"),
            SizedBox(width: 15,),
            Image.asset("images/twiter.png"),
          ],
        ),



      ],

    );
  }

  _bottomView() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Text("By signing in, creating an account, or checking out as a Guest, you are agreeing to our Terms of Use and our Privacy Policy",
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: ScreenUtil().setSp(12)
        ),),
    );
  }
}
