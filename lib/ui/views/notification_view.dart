import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/util/utility.dart';
import 'package:book_store/core/viewmodels/notification_view_model.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class NotificationView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationViewState();
  }
}

class NotificationViewState extends State<NotificationView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<NotificationViewModel>(
      onModelReady: (model) {},
      builder: (context, model, child) {
        return Scaffold(
          body: _body(model),
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            centerTitle: true,
            leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                )),
            title: Text(
              'Notification',
              style: TextStyle(color: HexColor('#000000')),
            ),
          ),
        );
      },
    );
  }

  _body(NotificationViewModel model) {
    return Container(
      padding: EdgeInsets.all(10),
      child: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Today',
                  style: TextStyle(
                    color: HexColor('#242126'),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17),
                  ),
                ),
                Text(
                  '2',
                  style: TextStyle(
                    color: HexColor('#00D6D8'),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          todayView(),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  'This Week',
                  style: TextStyle(
                    color: HexColor('#242126'),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17),
                  ),
                ),
                Text(
                  '3',
                  style: TextStyle(
                    color: HexColor('#00D6D8'),
                    fontWeight: FontWeight.bold,
                    fontSize: ScreenUtil().setSp(17),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          thisWeekView(),
        ],
      ),
    );
  }

  todayView() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: 3,
        physics: BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Slidable(
              actionPane: SlidableDrawerActionPane(),
              actionExtentRatio: 0.25,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    height: ScreenUtil().setHeight(100),
                    width: ScreenUtil().setHeight(100),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Container(
                              height: ScreenUtil().setHeight(100),
                              width: ScreenUtil().setHeight(100),
                              child: CachedNetworkImage(
                                imageUrl:
                                "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                                fit: BoxFit.fill,
                                placeholder: (context, url) => CommonLoader(
                                  size: small,
                                ),
                                errorWidget: (context, url, error) => Icon(
                                  Icons.broken_image,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white
                            ),
                            child: Image.asset('images/message.png'),
                          ),
                        )
                      ],


                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            'Robin\t',
                            style: TextStyle(
                                color: HexColor('#242126'),
                                fontSize: ScreenUtil().setSp(13),
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            'Mentioned you in a comment :',
                            style: TextStyle(
                              color: HexColor('#AFC1C4'),
                              fontSize: ScreenUtil().setSp(12),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            "@jeans\t",
                            style: TextStyle(
                                color: HexColor('#00D6D8'),
                                fontSize: ScreenUtil().setSp(12),
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Great! :)",
                            style: TextStyle(
                                color: HexColor('#242126'),
                                fontSize: ScreenUtil().setSp(12),
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "2 hours ago",
                        style: TextStyle(
                            color: HexColor('#D1DDDF'), fontSize: ScreenUtil().setSp(12), fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              ),
              secondaryActions: <Widget>[
                InkWell(
                  onTap: () {
                    Utility.showToast('Delete');
                  },
                  child: Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: HexColor('#FF3838').withOpacity(.3),
                      ),
                      child: Image.asset('images/delete.png')),
                ),
              ],
            ),
          );
        });
  }

  thisWeekView() {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: 5,
        physics: BouncingScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.only(top: 10, bottom: 10),
            child: Slidable(
              actionPane: SlidableDrawerActionPane(),
              actionExtentRatio: 0.25,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    height: ScreenUtil().setHeight(100),
                    width: ScreenUtil().setHeight(100),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(2),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Container(
                              height: ScreenUtil().setHeight(100),
                              width: ScreenUtil().setHeight(100),
                              child: CachedNetworkImage(
                                imageUrl:
                                "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                                fit: BoxFit.fill,
                                placeholder: (context, url) => CommonLoader(
                                  size: small,
                                ),
                                errorWidget: (context, url, error) => Icon(
                                  Icons.broken_image,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white
                            ),
                            child: Image.asset('images/message.png'),
                          ),
                        )
                      ],


                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Your Facebook friend Suzan is on Booking as Suzan',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(
                            color: HexColor('#AFC1C4'),
                            fontSize: ScreenUtil().setSp(12),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Text(
                          "2 hours ago",
                          style: TextStyle(
                              color: HexColor('#D1DDDF'), fontSize: ScreenUtil().setSp(12), fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(25)),
                        color: HexColor("#00D6D8")),
                    child: Center(
                      child: Text(
                        'Follow',
                        style: TextStyle(
                            fontSize: ScreenUtil().setSp(12),
                            fontWeight: FontWeight.bold,
                            color: HexColor('#FFFFFF')),
                      ),
                    ),
                    height: ScreenUtil().setHeight(32),
                    width: ScreenUtil().setWidth(68),
                  ),
                ],
              ),
              secondaryActions: <Widget>[
                InkWell(
                  onTap: () {
                    Utility.showToast('Delete');
                  },
                  child: Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: HexColor('#FF3838').withOpacity(.3),
                      ),
                      child: Image.asset('images/delete.png')),
                ),
              ],
            ),
          );
        });
  }
}
