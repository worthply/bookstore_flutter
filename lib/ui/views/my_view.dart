import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/my_view_model.dart';
import 'package:book_store/ui/router.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/widget/common_loader.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class MyView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return MyViewState();
  }
}

class MyViewState extends State<MyView>{
  @override
  Widget build(BuildContext context) {

    return BaseView<MyViewModel>(
      builder: (context,model,child){
        return Scaffold(
          body: _body(),
        );
      },
    );
  }

  _body() {
    return ListView(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.all(10),
      children: <Widget>[
        SizedBox(height: 30,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text("My",style: TextStyle(
                color: HexColor('#242126'),
                fontSize: ScreenUtil().setSp(44),
                fontWeight: FontWeight.bold
            ),),
            InkWell(
              onTap: (){
                Navigator.pushNamed(context, routeNotificationView);
              },
              child: Container(
                height: 25,
                width:30,
                child: Stack(
                  children: <Widget>[
                    Align(alignment: Alignment.bottomCenter,
                        child: Icon(Icons.notifications_none)),
                    Align(
                      alignment: Alignment.topRight,
                      child: Container(height: 7,width: 7,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: HexColor('#00D6D8')

                      ),),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
        SizedBox(height: 30),
        Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 10),
              child: Container(
                padding: EdgeInsets.all(2),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: Container(
                    height: ScreenUtil().setHeight(100),
                    width: ScreenUtil().setHeight(100),
                    child: CachedNetworkImage(
                      imageUrl: "https://content-static.upwork.com/uploads/2014/10/02123010/profilephoto_goodcrop.jpg",
                      fit: BoxFit.fill,
                      placeholder: (context, url) =>
                          CommonLoader(
                            size: small,
                          ),
                      errorWidget: (context, url, error) =>
                          Icon(
                            Icons.broken_image,
                          ),
                    ),
                  ),
                ),

              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                  decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(25)), color: HexColor("C4A12D").withOpacity(.1)),
                  child: Text('Gold',
                    style:TextStyle(
                      color: HexColor('#C4A12D')
                    ) ,),

                ),
                SizedBox(height: 10,),
                Text("JEANS",style: TextStyle(
                    color: HexColor('#242126'),
                    fontSize: ScreenUtil().setSp(24),
                    fontWeight: FontWeight.bold
                ),),
                SizedBox(height: 10,),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Follow",style: TextStyle(
                            color: HexColor('#D1DDDF'),
                            fontSize: ScreenUtil().setSp(17),
                            fontWeight: FontWeight.bold
                        ),),
                        Text("106",style: TextStyle(
                            color: HexColor('#242126'),
                            fontSize: ScreenUtil().setSp(17),
                            fontWeight: FontWeight.bold
                        ),),
                      ],
                    ),
                    SizedBox(width: 30,),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Following",style: TextStyle(
                            color: HexColor('#D1DDDF'),
                            fontSize: ScreenUtil().setSp(17),
                            fontWeight: FontWeight.bold
                        ),),
                        Text("95",style: TextStyle(
                            color: HexColor('#242126'),
                            fontSize: ScreenUtil().setSp(17),
                            fontWeight: FontWeight.bold
                        ),),
                      ],
                    ),
                    SizedBox(width: 30,),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text("Post",style: TextStyle(
                            color: HexColor('#D1DDDF'),
                            fontSize: ScreenUtil().setSp(17),
                            fontWeight: FontWeight.bold
                        ),),
                        Text("23",style: TextStyle(
                            color: HexColor('#242126'),
                            fontSize: ScreenUtil().setSp(17),
                            fontWeight: FontWeight.bold
                        ),),
                      ],
                    ),

                  ],
                )
              ],
            ),

          ],
        ),
        SizedBox(height: 30),
        Container(
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(topLeft:Radius.circular(10),topRight: Radius.circular(10) ),
            color: HexColor('#00D6D8')
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text('Members Card',
              style: TextStyle(
                fontSize: ScreenUtil().setSp(12),
                fontWeight: FontWeight.bold,
                color: HexColor('#FFFFFF'),

              ),),
              Text(' Payments',
              style: TextStyle(
                fontSize: ScreenUtil().setSp(10),
                fontWeight: FontWeight.bold,
                color: HexColor('#FFFFFF'),

              ),),

            ],
          ),
        ),
        SizedBox(height: 30,),
        Text('My Purchases',style: TextStyle(
          color: HexColor('#D1DDDF'),
          fontSize: ScreenUtil().setSp(14),
          fontWeight: FontWeight.bold
        ),),
        SizedBox(height: 20,),
        Row(
          children: <Widget>[
            Text('Update',style: TextStyle(
                color: HexColor('#242126'),
                fontSize: ScreenUtil().setSp(17),
                fontWeight: FontWeight.bold
            ),),
            Expanded(child: Container()),
            Text('3',style: TextStyle(
                color: HexColor('#00D6D8'),
                fontSize: ScreenUtil().setSp(17),
                fontWeight: FontWeight.bold
            ),),
            Icon(Icons.navigate_next,color: HexColor('D1DDDF'),),
            SizedBox(width: 10,),

          ],
        ),
        SizedBox(height: 15,),
        Row(
          children: <Widget>[
            Text('Book',style: TextStyle(
                color: HexColor('#242126'),
                fontSize: ScreenUtil().setSp(17),
                fontWeight: FontWeight.bold
            ),),
            Expanded(child: Container()),
            Text('32',style: TextStyle(
                color: HexColor('#D1DDDF'),
                fontSize: ScreenUtil().setSp(17),
                fontWeight: FontWeight.bold
            ),),
            Icon(Icons.navigate_next,color: HexColor('D1DDDF'),),
            SizedBox(width: 10,),

          ],
        ),
        SizedBox(height: 15,),
        Row(
          children: <Widget>[
            Text('Audio Book',style: TextStyle(
                color: HexColor('#242126'),
                fontSize: ScreenUtil().setSp(17),
                fontWeight: FontWeight.bold
            ),),
            Expanded(child: Container()),
            Text('13',style: TextStyle(
                color: HexColor('#D1DDDF'),
                fontSize: ScreenUtil().setSp(17),
                fontWeight: FontWeight.bold
            ),),
            Icon(Icons.navigate_next,color: HexColor('D1DDDF'),),
            SizedBox(width: 10,),

          ],
        ),
        SizedBox(height: 30),SizedBox(height: 30),
        Text('Account',style: TextStyle(
            color: HexColor('#D1DDDF'),
            fontSize: ScreenUtil().setSp(14),
            fontWeight: FontWeight.bold
        ),),
        SizedBox(height: 10,),
        Row(
          children: <Widget>[
            Text('Edit Profile',style: TextStyle(
                color: HexColor('#242126'),
                fontSize: ScreenUtil().setSp(17),
                fontWeight: FontWeight.bold
            ),),
            Expanded(child: Container()),
            Icon(Icons.navigate_next,color: HexColor('D1DDDF'),),
            SizedBox(width: 10,),

          ],
        ),
        SizedBox(height: 15,),
        Row(
          children: <Widget>[
            Text('Share your profile',style: TextStyle(
                color: HexColor('#242126'),
                fontSize: ScreenUtil().setSp(17),
                fontWeight: FontWeight.bold
            ),),
            Expanded(child: Container()),
            Icon(Icons.navigate_next,color: HexColor('D1DDDF'),),
            SizedBox(width: 10,),

          ],
        ),

      ],
    );
  }
}