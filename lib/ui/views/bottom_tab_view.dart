import 'package:book_store/core/util/hex_color.dart';
import 'package:book_store/core/viewmodels/bottom_tab_mView.dart';
import 'package:book_store/ui/views/feed_view.dart';
import 'package:book_store/core/viewmodels/signin_mView.dart';
import 'package:book_store/ui/views/base_view.dart';
import 'package:book_store/ui/views/home_view.dart';
import 'package:book_store/ui/views/library_view.dart';
import 'package:book_store/ui/views/my_view.dart';
import 'package:book_store/ui/views/search_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class BottomTabView extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {

    return BottomTabViewState();
  }

}

class BottomTabViewState extends State<BottomTabView> {
  @override
  Widget build(BuildContext context) {
    return BaseView<BottomTabViewModel>(
      onModelReady: (model) {},
      builder: (context, model, child) {
        return Scaffold(
          bottomNavigationBar: _bottomBarView(model),
          body: _body(model),
        );
      },
    );
  }

  _bottomBarView(BottomTabViewModel model) {
    return BottomAppBar(

      color: HexColor("#FFFFFF"),
      child: Container(
        padding: EdgeInsets.only(left: 15,right: 15),
        decoration: BoxDecoration(borderRadius: BorderRadius.vertical(bottom: Radius.circular(15))),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            InkWell(
              onTap: () {
                model.controller.jumpToPage(0);
                //controller.animateToPage(1, duration: Duration(milliseconds: 700), curve: Curves.ease);
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  model.currentPosition==0
                      ?Container(
                    height: 2,
                    width: MediaQuery.of(context).size.width/6,
                    color: HexColor("#00D6D8"),
                  )
                      :Container(width: MediaQuery.of(context).size.width/6,),
                  SizedBox(height: 10,),
                  Image(
                    image: AssetImage(model.currentPosition==0?"images/home_colored.png":"images/home_grey.png"),
                  ),
                  SizedBox(height: 7),
                  Text(
                    "Home",
                    style: TextStyle(color: HexColor(model.currentPosition==0?"#00D6D8":"#D1DDDF"),
                        fontSize: ScreenUtil().setSp(15)),
                  )
                ],
              ),
            ),
            InkWell(
              onTap: () {
                model.controller.jumpToPage(1);
                //controller.animateToPage(1, duration: Duration(milliseconds: 700), curve: Curves.ease);
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  model.currentPosition==1
                      ?Container(
                    height: 2,
                    width: MediaQuery.of(context).size.width/6,
                    color: HexColor("#00D6D8"),
                  )
                      :Container(width: MediaQuery.of(context).size.width/6,),
                  SizedBox(height: 10,),
                  Image(
                    image: AssetImage(model.currentPosition==1?"images/feed_colored.png":"images/feed_grey.png"),
                  ),
                  SizedBox(height: 7),
                  Text(
                    "Feed",
                    style: TextStyle(color: HexColor(model.currentPosition==1?"#00D6D8":"#D1DDDF"),
                    fontSize: ScreenUtil().setSp(15)),
                  )
                ],
              ),
            ),
            InkWell(
              onTap: () {
                model.controller.jumpToPage(2);
                //controller.animateToPage(1, duration: Duration(milliseconds: 700), curve: Curves.ease);
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  model.currentPosition==2
                      ?Container(
                    height: 2,
                    width: MediaQuery.of(context).size.width/6,
                    color: HexColor("#00D6D8"),
                  )
                      :Container(width: MediaQuery.of(context).size.width/6,),
                  SizedBox(height: 10,),
                  Image(
                    image: AssetImage(model.currentPosition==2?"images/search_colored.png":"images/searcg_grey.png"),
                  ),
                  SizedBox(height: 7),
                  Text(
                    "Search",
                    style: TextStyle(color: HexColor(model.currentPosition==2?"#00D6D8":"#D1DDDF"),
                        fontSize: ScreenUtil().setSp(15)),
                  )
                ],
              ),
            ),
            InkWell(
              onTap: () {
                model.controller.jumpToPage(3);
                //controller.animateToPage(1, duration: Duration(milliseconds: 700), curve: Curves.ease);
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  model.currentPosition==3
                      ?Container(
                    height: 2,
                    width: MediaQuery.of(context).size.width/6,
                    color: HexColor("#00D6D8"),
                  )
                      :Container(width: MediaQuery.of(context).size.width/6,),
                  SizedBox(height: 10,),
                //  _currentPosition == 1
                  Image(
                    image: AssetImage(model.currentPosition==3?"images/library_colored.png":"images/library_grey.png"),
                  ),
                  SizedBox(height: 7),
                  Text(
                    "Library",
                    style: TextStyle(color: HexColor(model.currentPosition==3?"#00D6D8":"#D1DDDF"),
                        fontSize: ScreenUtil().setSp(15)),
                  )
                ],
              ),
            ),
            InkWell(
              onTap: () {
                model.controller.jumpToPage(4);
                //controller.animateToPage(1, duration: Duration(milliseconds: 700), curve: Curves.ease);
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  model.currentPosition==4
                      ?Container(
                height: 2,
                width: MediaQuery.of(context).size.width/6,
                color: HexColor("#00D6D8"),
              )
                      :Container(width: MediaQuery.of(context).size.width/6,),
                  SizedBox(height: 10,),
                  Image(
                    image: AssetImage(model.currentPosition==4?"images/my_colored.png":"images/my_grey.png"),
                  ),
                  SizedBox(height: 7),
                  Text(
                    "My",
                    style: TextStyle(color: HexColor(model.currentPosition==4?"#00D6D8":"#D1DDDF"),
                        fontSize: ScreenUtil().setSp(15)),
                  )
                ],
              ),
            ),

          ],
        ),
      ),

    );
  }

  _body(BottomTabViewModel model) {
    return PageView(
      controller: model.controller,
      physics: NeverScrollableScrollPhysics(),
      onPageChanged: model.onPageViewChange,
      children: <Widget>[
        HomeView(),
        FeedView(),
        SearchView(),
        LibraryView(),
        MyView(),

      ],
    );
  }


}