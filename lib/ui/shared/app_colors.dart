import 'package:flutter/material.dart';

const Color primaryColor = const Color(0xFF479DD6);
const Color backgroundColor = Colors.white;
const Color backgroundColorSecond = const Color(0xFFFCFCFC);
const Color commentColor = Color.fromARGB(255, 255, 246, 196);
const Color blueButtonColor = const Color(0xFF479DD6);
const Color pinkButtonColor = Colors.grey;
const Color blueColor = const Color(0xFF479DD6);
const Color lightBlueColor = const Color(0xFF00d8ff);
const Color greenColor = const Color(0xFF4fcb78);
const Color greenColorDark = const Color(0xFF44AE57);