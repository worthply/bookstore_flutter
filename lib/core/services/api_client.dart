import 'dart:async';
import 'package:http/http.dart' as http;

class ApiClient {

  static const BASE_URL = "http://162.144.103.243/~kattha/katha-webservice/";
  Future<http.Response> getMethod(
      String url, Map<String, String> headers) async {
    print(url);
    var response = await http.get(url, headers: headers);
    print(response.body.toString());
    return response;
  }

  Future<String> postMethod(String method, var body) async {
    print(body);
    print(BASE_URL + method);
    var response = await http.post(BASE_URL + method,
        body: body);
    print(response.body.toString());
    return response.body.toString();
  }

  Future<http.Response> patchMethod(String method, var body,
      Map<String, String> headers) async {
    print(body);
    print(BASE_URL + method);
    var response = await http.patch(BASE_URL + method,
        body: body, headers: headers);
    return response;
  }

  Future<http.Response> postMethodMultipart(var request) async {
    http.Response response =
        await http.Response.fromStream(await request.send());
    print(response.body);
    return response;
  }

}
