import 'package:book_store/core/viewmodels/base_model.dart';
import 'package:flutter/cupertino.dart';

class BottomTabViewModel extends BaseModel{
  PageController controller = PageController();
  double _currentPosition = 0;

  double get currentPosition => _currentPosition;

  set currentPosition(double value) {
    _currentPosition = value;
    notifyListeners();
  }

  onPageViewChange(int page) {
    currentPosition = page.toDouble();
    print("Current Position="+currentPosition.toString());

  }

}