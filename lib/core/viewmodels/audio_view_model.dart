import 'package:book_store/core/viewmodels/base_model.dart';

class  AudioViewModel extends BaseModel{
  String readText= 'Bundling has also been used to segment the used book market. Each combination of a textbook and supplemental items receives a separate ISBN. A single textbook could therefore have dozens of ISBNs that denote different combinations of supplements packaged with that particular book. When a bookstore attempts to track down used copies of textbooks, they will search for the ISBN the course instructor orders, which will locate only a subset of the copies of the textbook';
  int _speed=1;
  bool _backgroundMode=false;
  String _voice="0";
  bool _showSetting=false;


  bool get showSetting => _showSetting;

  set showSetting(bool value) {
    _showSetting = value;
    notifyListeners();
  }

  String get voice => _voice;

  set voice(String value) {
    _voice = value;
    notifyListeners();
  }

  bool get backgroundMode => _backgroundMode;

  set backgroundMode(bool value) {
    _backgroundMode = value;
    notifyListeners();
  }

  int get speed => _speed;

  set speed(int value) {
    _speed = value;
    notifyListeners();
  }

}