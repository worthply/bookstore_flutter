import 'package:book_store/core/viewmodels/base_model.dart';

class FilterViewModel extends BaseModel{
  bool _boolFeatured=false;
  bool _boolLowToHigh=false;
  bool _boolHighToLow=false;
  bool _boolCustomerReview=false;
  bool _boolPublicationDate=false;

  bool _biographies=false;
  bool _literature=false;
  bool _literary=false;
  bool _memories=false;
  bool _suspense=false;

  bool _ratFour=false;
  bool _ratThree=false;
  bool _ratTwo=false;


  bool get boolLowToHigh => _boolLowToHigh;

  set boolLowToHigh(bool value) {
    _boolLowToHigh = value;
    notifyListeners();
  }

  bool get boolFeatured => _boolFeatured;

  set boolFeatured(bool value) {
    _boolFeatured = value;
    notifyListeners();
  }

  bool get boolHighToLow => _boolHighToLow;

  set boolHighToLow(bool value) {
    _boolHighToLow = value;
    notifyListeners();
  }

  bool get boolCustomerReview => _boolCustomerReview;

  set boolCustomerReview(bool value) {
    _boolCustomerReview = value;
    notifyListeners();
  }

  bool get boolPublicationDate => _boolPublicationDate;

  set boolPublicationDate(bool value) {
    _boolPublicationDate = value;
    notifyListeners();
  }

  bool get suspense => _suspense;

  set suspense(bool value) {
    _suspense = value;
    notifyListeners();
  }

  bool get memories => _memories;

  set memories(bool value) {
    _memories = value;
    notifyListeners();
  }

  bool get literary => _literary;

  set literary(bool value) {
    _literary = value;
    notifyListeners();
  }

  bool get literature => _literature;

  set literature(bool value) {
    _literature = value;
    notifyListeners();
  }

  bool get biographies => _biographies;

  set biographies(bool value) {
    _biographies = value;
    notifyListeners();
  }

  bool get ratTwo => _ratTwo;

  set ratTwo(bool value) {
    _ratTwo = value;
    notifyListeners();
  }

  bool get ratThree => _ratThree;

  set ratThree(bool value) {
    _ratThree = value;
    notifyListeners();
  }

  bool get ratFour => _ratFour;

  set ratFour(bool value) {
    _ratFour = value;
    notifyListeners();
  }


}