import 'dart:io';

import 'package:book_store/core/viewmodels/base_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
class ReadViewModel extends BaseModel{

  List<TextSelection> markList = List();

  bool _showNotes=false;

  bool _showNewPost=false;

  bool _showMenu=false;

  bool _scrolling=false;

  String _voice="0";


  String get voice => _voice;

  set voice(String value) {
    _voice = value;
    notifyListeners();
  }

  bool get scrolling => _scrolling;

  set scrolling(bool value) {
    _scrolling = value;
    notifyListeners();
  }

  bool get showMenu => _showMenu;

  set showMenu(bool value) {
    _showMenu = value;
    notifyListeners();
  }

  List<File> _imageList=List();

  List<File> get imageList => _imageList;

  set imageList(List<File> value) {
    _imageList = value;
    notifyListeners();
  }
  File _bigImage;

  File get bigImage => _bigImage;

  set bigImage(File value) {
    _bigImage = value;
    notifyListeners();
  }



  bool get showNewPost => _showNewPost;

  set showNewPost(bool value) {
    _showNewPost = value;
    notifyListeners();
  }

  bool get showNotes => _showNotes;

  set showNotes(bool value) {
    _showNotes = value;
    notifyListeners();
  }

  // open camera for profile photo
  openCamera() async {
    imageList.insert(1, await ImagePicker.pickImage(source: ImageSource.camera));
    bigImage=imageList[1];
    notifyListeners();
  }

  //open gallery for profile photo.
  openGallery() async {
    imageList.insert(1, await ImagePicker.pickImage(source: ImageSource.gallery));
    bigImage=imageList[1];
    notifyListeners();
  }

}