import 'package:book_store/core/viewmodels/base_model.dart';

class SignUpViewMode extends BaseModel{
  bool _check=false;

  bool get check => _check;

  set check(bool value) {
    _check = value;
    notifyListeners();
  }

}