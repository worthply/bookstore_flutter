

import 'package:shared_preferences/shared_preferences.dart';

class PreferenceConnector {
  static const USER_ID = 'user_id';
  static const STORY_ID = 'story_id';
  static const FACEBOOK_ID = 'facebook_id';
  static const GOOGLE_ID = 'google_id';
  static const ACCESS_TOKEN = 'access_Token';
  static const VIEWED_INTRO = 'view_intro';
  static const VIDEO_THUMB = 'video_thumb';
  static const VIDEO_ID = 'video_id';


  _getSharedPreference() async {
    return await SharedPreferences.getInstance();
  }

  Future<String> getString(String key) async {
    SharedPreferences prefs = await _getSharedPreference();
    return prefs.getString(key) ?? '';
  }

  Future<int> getInt(String key) async {
    SharedPreferences prefs = await _getSharedPreference();
    return prefs.getInt(key) ?? -1;
  }

  Future<bool> getBool(String key) async {
    SharedPreferences prefs = await _getSharedPreference();
    return prefs.getBool(key) ?? false;
  }

  void setString(String key, String value) async {
    SharedPreferences prefs = await _getSharedPreference();
    prefs.setString(key, value);
  }

  void setInt(String key, int value) async {
    SharedPreferences prefs = await _getSharedPreference();
    prefs.setInt(key, value);
  }

  void setBool(String key, bool value) async {
    SharedPreferences prefs = await _getSharedPreference();
    prefs.setBool(key, value);
  }

  void clear() async {
    SharedPreferences prefs = await _getSharedPreference();
    prefs.clear();
  }
}
