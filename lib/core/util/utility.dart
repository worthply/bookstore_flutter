import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'dart:math' as Math;
import 'package:path/path.dart' as pathP;

fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}

class Utility {

  static void showInfoDialog(BuildContext context, String msg) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Info!"),
            content: Text(msg),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Ok"))
            ],
          );
        });
  }

  static void showSuccessSnackBar(
      GlobalKey<ScaffoldState> scaffoldState, String message,
      {Color color}) {
    if (color == null) {
      color = Colors.green[900];
    }
    final snackBar = SnackBar(
      content: Text(message),
      backgroundColor: color,
    );
    // Find the Scaffold in the Widget tree and use it to show a SnackBar!
    if (scaffoldState.currentState != null) {
      scaffoldState.currentState.showSnackBar(snackBar);
    }
  }

  static void showErrorSnackBar(
      GlobalKey<ScaffoldState> scaffoldState, String message,
      {Color color}) {
    if (color == null) {
      color = Colors.red[900];
    }
    final snackBar = SnackBar(
      content: Text(message),
      backgroundColor: color,
    );
    // Find the Scaffold in the Widget tree and use it to show a SnackBar!
    if (scaffoldState.currentState != null) {
      scaffoldState.currentState.removeCurrentSnackBar();
      scaffoldState.currentState.showSnackBar(snackBar);
    }
  }

  static void removeSnackBar(GlobalKey<ScaffoldState> scaffoldState) {
    if (scaffoldState.currentState != null) {
      scaffoldState.currentState.removeCurrentSnackBar();
    }
  }

  static String getFilterNumber(String x) {
    var lastThree = x.substring(x.length - 3);
    var otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers != '') lastThree = ',' + lastThree;
    var res = otherNumbers.replaceAll(RegExp("\\B(?=(\\d{2})+(?!\\d))"), ",") +
        lastThree;
    return res;
  }

  static String getCardType(String creditCardNumber){
      RegExp regVisa = new RegExp("^4[0-9]{12}(?:[0-9]{3})?\$");
      RegExp regMaster = new RegExp("^5[1-5][0-9]{14}\$");
      RegExp regExpress = new RegExp("^3[47][0-9]{13}\$");
      RegExp regDiners = new RegExp("^3(?:0[0-5]|[68][0-9])[0-9]{11}\$");
      RegExp regDiscover = new RegExp("^6(?:011|5[0-9]{2})[0-9]{12}\$");
      RegExp regJCB= new RegExp("^(?:2131|1800|35\\d{3})\\d{11}\$");


      if(regVisa.hasMatch(creditCardNumber))
        return "visa";
      else if (regMaster.hasMatch(creditCardNumber))
        return "mastercard";
      else  if (regExpress.hasMatch(creditCardNumber))
        return "amex";
      else if (regDiners.hasMatch(creditCardNumber))
        return "diners";
      else if (regDiscover.hasMatch(creditCardNumber))
        return "discover";
      else   if (regJCB.hasMatch(creditCardNumber))
        return "jcb";
      else
        return "invalid";
  }

  static myHashCode(String str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
      hash = str.codeUnitAt(i) + ((hash << 5) - hash);
    }
    print(hash);
    return hash;
  }

  static String toHex(String str){
    int value = myHashCode(str);
    String hex = value.toRadixString(16).padLeft(2, '0').toUpperCase();
    hex = '${hex[0]}${hex[0]}${hex[1]}${hex[1]}${hex[2]}${hex[2]}';
    print(hex);
    return hex;

  }

  static Future<bool> checkNetwork() async{
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile||connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  /*static String updatedProfile(String profileUrl){
    //?datetime={currentTime}
    String time = DateTime.now().toIso8601String();
    return '$profileUrl?datetime=$time';
  }*/

  static showToast(String message){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }

  static centerToast(String message){
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.grey,
        textColor: Colors.white,
        fontSize: 16.0
    );
  }


  static intToRGB(int i){
    var c = (i & 0x00FFFFFF)
        .toString()
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
  }

  static transparentStatusBar(){
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
      statusBarColor: Colors.transparent,
    ));
  }

  static bool networkStatus(){
    bool showNoInternet = false;
     Connectivity().onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        Utility.centerToast("No Internet connection");
         showNoInternet = true;
      } else {
        Utility.centerToast("Internet connected");
         showNoInternet = false;
      }
    });
    return showNoInternet;
  }

}
