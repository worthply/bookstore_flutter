import 'package:book_store/core/services/api_client.dart';
import 'package:book_store/core/services/repository.dart';
import 'package:book_store/core/viewmodels/audio_view_model.dart';
import 'package:book_store/core/viewmodels/bookshelf_view_model.dart';
import 'package:book_store/core/viewmodels/bottom_tab_mView.dart';
import 'package:book_store/core/viewmodels/content_tab_view_model.dart';
import 'package:book_store/core/viewmodels/details_view_model.dart';
import 'package:book_store/core/viewmodels/filter_view_model.dart';
import 'package:book_store/core/viewmodels/menu_view_model.dart';
import 'package:book_store/core/viewmodels/my_post_view_model.dart';
import 'package:book_store/core/viewmodels/notification_view_model.dart';
import 'package:book_store/core/viewmodels/read_view_model.dart';
import 'package:book_store/core/viewmodels/reading_now_view_model.dart';
import 'package:book_store/core/viewmodels/search_author_view_model.dart';
import 'package:book_store/core/viewmodels/search_book_view_model.dart';
import 'package:book_store/core/viewmodels/search_category_view_model.dart';
import 'package:book_store/core/viewmodels/search_charts_view_model.dart';
import 'package:book_store/core/viewmodels/feed_story_view_model.dart';
import 'package:book_store/core/viewmodels/feed_view_model.dart';
import 'package:book_store/core/viewmodels/home_View_model.dart';
import 'package:book_store/core/viewmodels/library_view_mode.dart';
import 'package:book_store/core/viewmodels/search_keyword_view_model.dart';
import 'package:book_store/core/viewmodels/search_more_view_model.dart';
import 'package:book_store/core/viewmodels/my_view_model.dart';
import 'package:book_store/core/viewmodels/search_result_view_model.dart';
import 'package:book_store/core/viewmodels/search_view_model.dart';
import 'package:book_store/core/viewmodels/sign-up_mView.dart';
import 'package:book_store/core/viewmodels/signin_mView.dart';
import 'package:book_store/core/viewmodels/splash_mView.dart';
import 'package:book_store/core/viewmodels/welcome_mView.dart';
import 'package:book_store/ui/views/AudioView.dart';
import 'package:book_store/ui/views/content_tab_view.dart';
import 'package:book_store/ui/views/details_view.dart';

import 'package:get_it/get_it.dart';

GetIt locator = GetIt();

void setupLocator() {
  locator.registerLazySingleton(() => ApiClient());
  locator.registerLazySingleton(() => Repository());

  locator.registerFactory(() => SplashViewModel());
  locator.registerFactory(() => WelcomeModelView());
  locator.registerFactory(() => SignInModelView());
  locator.registerFactory(() => SignUpViewMode());
  locator.registerFactory(() => BottomTabViewModel());
  locator.registerFactory(() => HomeViewModel());
  locator.registerFactory(() => FeedViewModel());
  locator.registerFactory(() => SearchViewModel());
  locator.registerFactory(() => LibraryViewModel());
  locator.registerFactory(() => MyViewModel());
  locator.registerFactory(() => FeedStoryViewMode());
  locator.registerFactory(() => CategorySearchViewModel());
  locator.registerFactory(() => ChartSearchViewModel());
  locator.registerFactory(() => MoreSearchViewModel());
  locator.registerFactory(() => SearchBookViewModel());
  locator.registerFactory(() => SearchAuthorViewModel());
  locator.registerFactory(() => SearchKeywordViewModel());
  locator.registerFactory(() => SearchResultViewModel());
  locator.registerFactory(() => FilterViewModel());
  locator.registerFactory(() => ReadingNowViewModel());
  locator.registerFactory(() => BookShelfViewMode());
  locator.registerFactory(() => MyPostViewModel());
  locator.registerFactory(() => DetailsViewModel());
  locator.registerFactory(() => NotificationViewModel());
  locator.registerFactory(() => ReadViewModel());
  locator.registerFactory(() => AudioViewModel());
  locator.registerFactory(() => MenuViewModel());
  locator.registerFactory(() => ContentTabViewModel());

}
